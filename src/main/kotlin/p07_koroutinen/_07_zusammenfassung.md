# Sonstiges
 * Actor
 * Zusammenarbeit mit Reaktiven Libraries (Webflux, RxJava, etc)
 
 
# Zusammenfassung

* __```suspend```__ konvertiert Funktionen zu Koroutinen
* Sequential by Default / Asynchronous explicitly
* Asynchronen Kommunikationsmustern als Library
* Einfache Integration in vorhandene asynchrone APIs
* Tooling / Debugging muss noch verbessert werden
* Stackless: Suspendierungen nur in Koroutinen möglich [(Red/Blue Code Problem)](http://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/)</li>

---

# Red-Blue-Problem

## Alles Routinen  
  ![](_red-blue-1.png)

---

# C() wird zur Koroutine
  ![](_red-blue-2.png)

---

## Ausblick

 * Stackfull-Koroutinen durch [__Quasar__](https://github.com/puniverse/quasar)

 * Oder durch Project [__Loom__](http://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html) ([Idee](https://www.youtube.com/watch?v=fpyub8fbrVE))
  
