package p07_ausblick

import kotlin.random.Random

typealias Dings = String

inline class GitHash(val value: String)

fun main() {
    val hamlet = if (Random.nextInt(1) == 0) ToBe() else NotToBe()

    val theAnswer =
        when (hamlet) {
            is ToBe -> "Just be it: ${hamlet.zahl}"
            is NotToBe -> "Nö, doch nix."
            // The compiler will complain if a case is missing
        }

    println(theAnswer)

    val gitHash = GitHash("a123")
    val number = gitHash.value.toLong(16)
    println(number)
}

sealed class TheQuestion

class ToBe(val zahl: Int = 42) : TheQuestion()

class NotToBe() : TheQuestion()