package p01_intro

import mu.KLogging
import org.junit.jupiter.api.Test
import java.io.Serializable


class Wiederholung {

    interface I {
        val i: Int
    }

    abstract class A(val a: Int)
    data class B(var b: Int) : A(42), Serializable {
        companion object : I {
            override val i: Int
                get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

        }
    }


    open class C : A(4711) {
        companion object : KLogging()
    }

    class D : C()


    @Test
    fun `Welche (Java-)Funktionen hat B Wie könnte ich das rausbekommen`() {
        val b = B(1)
        b.hashCode()
        b.equals(b) // b  == b
        b.copy(12)

    }

    @Test
    fun `Welche Werte haben die Variablen x1 bis x4`() {
        val a1: A = B(42)
        val a2: A = C()

        val x1 = a1 is B //true
        val x2 = a1 as B // not null B
        val x3 = a2 as? B //null
        val x4 = a2 as B // Exception
    }

    @Test
    fun `Warum kompiliert das?`() {
        val a1: A = B(42)

        a1 as B

        println(a1.b)
    }


    @Test
    fun `Was muss ich tun damit das kompiliert`() {

        val i: I = B
        C.logger.error { "Error" }
    }

    @Test
    fun `Was ist das Fülle die Liste mit Daten`() {

        fun tuWas() = 7

        val list: MutableList<() -> Int> = mutableListOf()

        val adHoc = { 5 }
        list.add(adHoc)

        list.add({ 6 })

        val mehtodReference: () -> Int = ::tuWas
        list.add(mehtodReference)

        val meinObjectB = B(42)

        val propB: () -> Int = meinObjectB::b
        list.add(propB)

        for (function in list) {
            println(function())
        }

    }

    @Test
    fun `Was ist das für eine Funktion und wie rufe ich diese auf`() {
        fun A.b(c: C): I? = null

        C().b(C())

        fun String.toZahl(basis: Int = 10): Number? = null

        val zahl = "42".toZahl() ?: 0
    }

    @Test
    fun `Noch eine komische Funktion - was macht die`() {
        fun <T>((T, T) -> Unit).partial(i1: T) = fun(i2: T) {
            this(i1, i2)
        }

        fun printAundB(a: Int, b: Int) {
            println("$a und $b")
        }

        printAundB(1,2)

        val print1undB = ::printAundB.partial(1)


        print1undB(2)



    }

}

