package p07_koroutinen._08_ws.loesung

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.request.header
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import mu.KotlinLogging
import java.time.Duration

data class ChatMessage(val clientId: String, val msg: String)

fun Application.webSockets() {
    val logger = KotlinLogging.logger("server")

    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(60)
    }

    val broadcastChannel = BroadcastChannel<ChatMessage>(1)

    routing {
        webSocket("/ping") {
            val clientId = call.request.header("client.id")
            logger.info("Connect $clientId")

            for (message in incoming) {
                if (message is Frame.Text) {
                    val text = message.readText()
                    println("Receive message $text from $clientId")
                    outgoing.send(Frame.Text("pong to $clientId"))
                }
            }
            logger.info("Disconnect $clientId")

        }

        webSocket("/chat") {
            val clientId = call.request.header("client.id") ?: "unknown"
            logger.info("Connect $clientId")

            coroutineScope {
                val channel = broadcastChannel.openSubscription()
                try {

                    val broadcastJob = launch {
                        for (chatMessage in channel) {
                            if (chatMessage.clientId != clientId) {
                                outgoing.send(Frame.Text("${chatMessage.msg} from ${chatMessage.clientId}"))
                            }
                        }
                    }

                    for (message in incoming) {
                        if (message is Frame.Text) {
                            val text = message.readText()
                            logger.info("Receive message $text from $clientId")
                            broadcastChannel.send(ChatMessage(clientId, text))
                        }
                    }

                    broadcastJob.cancel()

                } finally {
                    channel.cancel()
                }
            }

            logger.info("Disconnect $clientId")

        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::webSockets)
    server.start(wait = true)

}