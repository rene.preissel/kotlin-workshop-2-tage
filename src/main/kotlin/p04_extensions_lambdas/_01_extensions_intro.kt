package p04_extensions_lambdas

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import p04_extensions_lambdas.util.toURL
import java.net.URL

/**
 * ## Extensions Funktionen
 *
 * Extensions erweitern vorhandene Klassen/Interfaces um zusätzlichen Methoden und Properties
 * * Müssen explizit importiert werden
 * * Können keine vorhandenen Methoden überschreiben bzw. redefinieren
 * * Werden intern als statische Methoden implementiert (IDEA: `Tools/Kotlin/Show Kotlin Bytecode + Decompile`)
 */
class _01_extensions_intro {
    @Test
    fun `01 Einfache Extensions Funktion`() {
        class Person(val name: String, val email: String)
        data class PersonDTO(val name: String)

        //Extension Funktion definieren
        fun Person.toDTO(): PersonDTO = PersonDTO(name)

        val rene = Person("Rene", "rp@etosquare.de")

        // Extension Funktion aufrufen
        val reneDTO = rene.toDTO()

        println(reneDTO)
    }

    @Test
    fun `02 Extensions Funktionen als Ersatz für Util-Klassen`() {

        //siehe statischer Import
        val url = "http://localhost".toURL()

        assertEquals(URL("http://localhost"), url)
    }

    @Test
    fun `03 Extensions Funktionen für konkrete generische Parameter`() {

        fun List<Int>.sumAll(): Int =
            fold(0) { i1, i2 -> i1 + i2 }

        fun List<String>.sumAll(): Int =
            fold(0) { s1, s2 -> s1.toInt() + s2.toInt() }

        val sumInt = listOf(1, 2).sumAll()
        val sumString = listOf("1", "2").sumAll()

        assertEquals(3, sumInt)
        assertEquals(3, sumString)
    }

    @Test
    fun `04 Extensions Funktionen Nullable-Typen`() {

        fun List<Double>?.sumAll(): Double {
            if (this == null) {
                return 0.0
            }
            return fold(0.0) { i1, i2 -> i1 + i2 }
        }

        val nullList: List<Double>? = null
        assertEquals(0.0, nullList.sumAll())

        //oder auch
        val emptyList = nullList.orEmpty()
        assertTrue(emptyList.isEmpty())
    }

}


