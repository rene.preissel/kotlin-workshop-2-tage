package p06_dsl

import kotlinx.html.DIV
import kotlinx.html.a
import kotlinx.html.div
import kotlinx.html.h1
import kotlinx.html.h2
import kotlinx.html.li
import kotlinx.html.stream.appendHTML
import kotlinx.html.ul
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.StringWriter

/**
 * ## Kotlinx.html ist eine typsichere Builder-Api zum Erzeugen von HTML.
 */
class _01_kotlinx_html {

    @Test
    fun `01 Lambda With Receiver bilden die Grundlage von dieser DSL`() {

        // appendHTML ist eine Extension Funktion
        System.out.appendHTML()

            // Lambda with Receiver
            .div {
                h1 {
                    text("Features")
                }
                ul {
                    li { text("Extension Functions") }
                    li { text("Lambdas with receiver") }
                    li { text("Operator overloading") }
                }
            }
    }

    @Test
    fun `02 Extension Funktionen dienen zum erweitern der DSL`() {


        val html1 = StringWriter().apply {

            appendHTML()
                .div {
                    a("./link1") {
                        h2 { text("Menu 1") }
                    }
                    a("./link2") {
                        h2 { text("Menu 2") }
                    }
                }

        }.toString()


        //Extension Funktion erweitert die DSL
        fun DIV.menuEntry(title: String, href: String) =
            a(href) {
                h2 { text(title) }
            }

        val html2 = StringWriter().apply {
            appendHTML()
                .div {

                    //Eigene Funktion aufrufen
                    menuEntry(title = "Menu 1", href = "./link1")
                    menuEntry(title = "Menu 2", href = "./link2")

                }
        }.toString()

        assertEquals(html1, html2)
    }

    @Test
    fun `03 DSLMarker verhindert ungewollte Aufrufe`() {
        System.out.appendHTML()
            .div {
                ul {
                    li {
                        //Compile Error
//                        li {
//                            text("Extension Functions")
//                        }
                    }
                }
            }
    }
}


