package p06_dsl._06_svg_dsl.uebung

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.html.respondHtml
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.body
import kotlinx.html.unsafe
import p06_dsl._06_svg_dsl.loesung.RGB


/**
 * AUFGABE
 *
 * Level1
 * Erweitere die SVG-DSL um ein neues Element: Circle
 * <circle cx="100" cy="200" r="10" ...></circle>
 *
 * Level2
 * Verhindere, das man innerhalb des rect-Blockes die circle-Funktion aufrufen kann und umgekehrt.
 */
fun Application.main() {
    install(Routing) {
        get("/") {
            val svg = svg {
                width = 300
                rect(10, 20) {
                    stroke = RGB(0, 1, 1)
                    fill = RGB(255, 0, 0)
                }

                //Hier bitte einen Kreis ausgeben
                /*
                 circle( ... ) {
                    stroke = RGB(0, 1, 1)
                    fill = RGB(255, 0, 0)
                 }
                 */
            }

            call.respondHtml {
                body {
                    unsafe {
                        raw(svg)
                    }
                }
            }
        }

    }
}

fun main(args: Array<String>) {
    val server = embeddedServer(
        Netty, port = 8080, module = Application::main
    )
    server.start(wait = true)
}