package p02_oo_klassen

import org.junit.jupiter.api.Test

/**
 * AUFGABE
 * Ermittle den größten gemeinsamen Teiler und das kleinste gemeinsame Vielfache für zwei ganze Zahlen
 * Schreibe jeweils eine Funktion und sorge dafür, das beide Test erfolgreich laufen
 */
class _04a_ggT_kgV {
    /**
     *
     * siehe: https://de.wikipedia.org/wiki/Gr%C3%B6%C3%9Fter_gemeinsamer_Teiler
     * Euklidischer und steinscher Algorithmus
     */
    @Test
    fun `ggT für zwei ganze Zahlen`() {

//        assertThat(ggT(153, 81)).isEqualTo(9)
//        assertThat(ggT(81, 153)).isEqualTo(9)
//        assertThat(ggT(0, 153)).isEqualTo(153)
//        assertThat(ggT(153, 0)).isEqualTo(153)
    }

    /**
     * Berechnung über ggT
     * siehe: https://de.wikipedia.org/wiki/Kleinstes_gemeinsames_Vielfaches#Berechnung_%C3%BCber_den_gr%C3%B6%C3%9Ften_gemeinsamen_Teiler_(ggT)
     * kgV(z1,z2) = (z1 * z2) / ggT(z1,z2)
     */
    @Test
    fun `kgV für zwei ganze Zahlen`() {

//        assertThat(kgV(72, 120)).isEqualTo(360)
    }
}