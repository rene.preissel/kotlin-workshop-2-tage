package p02_oo_klassen._09b.loesung.refined


import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class BruchTest {

    @Test
    fun `Konstruktur setzt Zähler und Nenner`() {

        with(Bruch.of(3, 7)) {
            assertEquals(zaehler, 3)
            assertEquals(nenner, 7)
        }
    }

    @Test
    fun `Nenner 0 ist unzulässig`() {
        assertThrows<IllegalArgumentException> {

            Bruch.of(3, 0)
        }
    }

    @Test
    fun `Konstruktor kürzt, wo notwendig`() {

        with(Bruch.of(2, 6)) {
            assertEquals(zaehler, 1)
            assertEquals(nenner, 3)
        }
    }

    @Test
    fun `0-Brüche werden normiert`() {

        with(Bruch.of(0, 8)) {
            assertEquals(zaehler, 0)
            assertEquals(nenner, 1)
        }

    }

    @Test
    fun `Auch bei negativen Zahlen wird korrekt gekürzt`() {

        with(Bruch.of(-6, 8)) {
            assertEquals(zaehler, -3)
            assertEquals(nenner, 4)
        }

    }

    @Test
    fun `Brüche werden als gleich erkannt, wenn unterschiedliche ungekürzte Repräsentationen genutzt werden`() {
        assertEquals(Bruch.of(2, 6), Bruch.of(1, 3))
        assertEquals(Bruch.of(4, 6), Bruch.of(6, 9))
        assertEquals(Bruch.of(42, 132), Bruch.of(7, 22))
        assertEquals(Bruch.of(-5, 6), Bruch.of(-10, 12))
    }

    @Test
    fun `Größter gemeinsamer Teiler, wird korrekt berechnet`() {
        assertEquals(ggT(1, 1), 1)
        assertEquals(ggT(23, 23), 23)
        assertEquals(ggT(4, 4), 4)

        assertEquals(ggT(2, 6), 2)
        assertEquals(ggT(6, 2), 2)

        assertEquals(ggT(49, 7), 7)
        assertEquals(ggT(7, 49), 7)

        assertEquals(ggT((3 * 11 * 23).toLong(), (11 * 37).toLong()), 11)
        assertEquals(ggT(1, 23), 1)
    }

    @Test
    fun `Nur Positive Werte sind zulässig(1)`() {
        assertThrows<IllegalArgumentException> {
            ggT(0, 7)
        }
    }

    @Test
    fun `Nur Positive Werte sind zulässig(2)`() {
        assertThrows<IllegalArgumentException> {
            ggT(7, 0)
        }
    }

    @Test
    fun `Nur Positive Werte sind zulässig(3)`() {
        assertThrows<IllegalArgumentException> {
            ggT(-11, 7)
        }
    }

}
