package p02_oo_klassen._09b.loesung.simple

import java.lang.Math.abs
import java.util.*

class Bruch(zaehler: Long, nenner: Long) {

    var zaehler: Long = 0
        private set
    var nenner: Long = 0
        private set

    init {
        if (nenner < 1)
            throw IllegalArgumentException("Nenner nicht positiv: $nenner")

        if (zaehler == 0L) {
            this.zaehler = 0
            this.nenner = 1
        } else {
            val ggT = BruchUtils.ggT(abs(zaehler), nenner)

            this.zaehler = zaehler / ggT
            this.nenner = nenner / ggT
        }
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val bruch = o as Bruch?
        return zaehler == bruch!!.zaehler && nenner == bruch.nenner
    }

    override fun hashCode(): Int {
        return Objects.hash(zaehler, nenner)
    }

    override fun toString(): String {
        return "Bruch{" +
                "zaehler=" + zaehler +
                ", nenner=" + nenner +
                '}'.toString()
    }

}
