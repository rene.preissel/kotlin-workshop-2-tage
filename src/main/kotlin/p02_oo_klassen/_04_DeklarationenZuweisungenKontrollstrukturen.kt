package p02_oo_klassen


import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.lang.Exception
import java.lang.NumberFormatException
import java.time.DayOfWeek
import java.time.LocalDateTime
import java.time.Month
import java.util.*

/**
 * ## *Values* und *Variables*
 *
 * Kotlin unterscheidet zwischen *Values* und *Variables*: `val` und `var`.
 *
 * * `val` entspricht im wesentlichen einer `final` Deklararion in Java,
 *    d.h. es darf nur ein einziges Mal ein Wert zugeordnet werden.
 * * `var`'s hingegen dürfen jederzeit neue Werte zugewiesen bekommen.
 *
 * ## Kontrollstrukturen
 *
 * Die aus Java bekannten Kontrollstrukturen sind vorhanden, können aber teilweise auch als Statement benutzt werden
 *
 * * `if` - Fallunterscheidung
 * * `while` - Schleife
 * * `for` gibt es nur als Schleife über Iterables (Listen, etc)
 * * `when` - verbesserte `switch`-Anweisung
 *
 */
class _04_DeklarationenZuweisungenKontrollstrukturen {


    /**
     * ## Einfache Variablen und Werte- Deklarationen
     *
     * * Es wird immer ein `val` (oder `var`) vorangestellt.
     *   - Ausnahme: Parameter von Funktionen (siehe unten).
     *
     * * Die Typangabe steht, mit `:` abgetrennt, hinter dem Namen.
     *
     **/
    @Test
    fun `01 Simple deklaration`() {


        val theAnswer1: Int
        theAnswer1 = 42


        val theAnswer2: String = "zweiundvierzig"


        assertThat(theAnswer1).isEqualTo(42)
        assertThat(theAnswer2).isEqualTo("zweiundvierzig")

    }

    /**
     * ## `val` vs. `var`
     *
     * Das Keyword `final` gibt es in Kotlin nicht für Variablen,
     * statt dessen wird allen Deklarationen entweder `val`
     * oder `var` vorangestellt, um zu bestimmen, ob
     * Neuzuweisungen erlaubt sind.
     *
     */
    @Test
    fun `02 val vs var`() {


        val aValue: Int = 42
        // aValue = 4711        // keine erneute Zuweisung erlaubt


        var aVariable: Int = 11
        aVariable = 23


        assertThat(aValue).isEqualTo(42)
        assertThat(aVariable).isEqualTo(23)
    }


    /**
     * ## Funktionsparameter sind immer `val`
     *
     */
    @Test
    fun `03 bei Parametern ist val oder var nicht anzugeben, es gilt val`() {


        fun verdopple(n: Int): Int {

            // n = 4711     // Keine erneute Zuweisung erlaubt!

            return n + n
        }


        assertThat(verdopple(21)).isEqualTo(42)
    }


    /**
     * ## `val` heißt nicht Seiteneffekt-Frei!
     *
     * Ähnlich wie `final` in Java schützt `val` alleine nicht vor Seiteneffekten.
     *
     */
    @Test
    fun `04 Der Inhalt strukturierter Objekte kann auch bei Values nachträglich geändert werden`() {


        val aJavaArrayList: ArrayList<Int> = ArrayList()
        aJavaArrayList.add(42)


        assertThat(aJavaArrayList).contains(42)


        aJavaArrayList.clear()


        assertThat(aJavaArrayList).isEmpty()
    }

    /**
     * ## Type Inference
     *
     * Die Typeangaben kann (und sollte oft auch) weggelassen
     * werden wenn der Typ anhand des Zugewiesenen Wertes
     * ermittelt werden kann.
     *
     */
    @Test
    fun `05 Die Typangabe ist Optional, wenn der Typ anhand einer Zuweisung erkennbar ist`() {


        val theAnswer = 42


        assertThat(theAnswer).isEqualTo(42)
        assertThat(theAnswer is Int)
    }


    /**
     * ## If Statement
     *
     * `if` verhält ich wie in Java, kann aber auch als Statement benutzt werden
     * Dafür gibt es den `?`-Operator nicht in Kotlin
     */
    @Test
    fun `06 If kann als Statement benutzt werden`() {

        val frau = true

        val anrede = if (frau) "Frau" else "Mann"

        assertThat(anrede).isEqualTo("Frau")
    }

    /**
     * ## While  Schleifen
     *
     * `while` - Schleifen verhalten sich wie in Java
     */
    @Test
    fun `07 While Schleifen verhalten sich wie in Java`() {

        var month = Month.JANUARY

        while (month != Month.DECEMBER) {

            month = month.plus(1)

        }

        assertThat(month).isEqualTo(Month.DECEMBER)

        do {

            month = month.minus(1)

        } while(month != Month.JANUARY)

        assertThat(month).isEqualTo(Month.JANUARY)
    }


    /**
     * ## For Schleifen
     *
     * `for` - Schleifen können nur über Iterables laufen
     */
    @Test
    fun `08 For Schleifen sind nur über Iterables möglich`() {

        for(m in 1..12) {
            val month = Month.of(m)
            println(month)
        }
    }

    /**
     * ## Das `when`-Statement
     *
     * Das `when`-Statement stellt Fallunterscheidungen mit mehr als
     * zwei Zweigen übersichtlicher dar, als eine Folge von `if`-Statements.
     *
     * **Hinweis** Das `when`-Statement in Kotlin ist sehr mächtig,
     * z. B. bei Verwendung von `in`- oder `is`-Bedingungen
     * Hier werden nur ein paar einfache Fälle angerissen.
     */
    @Test
    fun `09 das when statement kann Fallunterscheidungen in Funktionen lesbarer machen`() {

        fun fib(n: Int): Int =

        // Fallunterscheidung über einen Parameter, hier `n`

            when (n) {
                1 -> 1
                2 -> 1
                else -> fib(n - 1) + fib(n - 2)
            }

        println("fib(5): ${fib(5)}")


        val hour = LocalDateTime.now().hour
        val day = LocalDateTime.now().dayOfWeek

        // Fallunterscheidung über freie Bedingungen

        when {
            hour == 17 ->
                println("Tea Time")

            hour == 11 && day == DayOfWeek.SUNDAY ->
                println("Brunch")

            else ->
                println("Weiss nicht recht")
        }

        when (hour) {
            in 8..11 -> println("Vormittag")
            in 12..18 -> println("Nachmittag")
            else -> println("Finstere Nacht")
        }
    }

    /**
     * ## Try-Statement
     *
     * `try/catch/finally` - ist ein Statement
     * * kein Catch mit mehreren Typen  (A|B) möglich
     */
    @Test
    fun `10 try als Statement`() {

        val value1 = try {
            Integer.parseInt("abc")
        } catch (e: NumberFormatException) {
            0
        }

        assertThat(value1).isEqualTo(0)

        val value2 = try {
            Integer.parseInt("abc")
        } catch (e: Exception) {
            when(e) {
                is NumberFormatException -> 0
                else -> -1
            }
        }

        assertThat(value2).isEqualTo(0)
    }

}

