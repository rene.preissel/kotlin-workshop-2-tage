package p02_oo_klassen

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.lang.Math.abs

class _04a_ggT_kgV_loesung {
    /**
     *
     * siehe: https://de.wikipedia.org/wiki/Gr%C3%B6%C3%9Fter_gemeinsamer_Teiler
     * Euklidischer und steinscher Algorithmus
     */
    @Test
    fun `ggT für zwei ganze Zahlen`() {
        fun ggT(z1: Int, z2: Int): Int =
            when {
                z1 == 0 -> abs(z2)
                z2 == 0 -> abs(z1)
                else -> {
                    var a = z1
                    var b = z2
                    do {
                        val r = a % b
                        a = b
                        b = r
                    } while (b != 0)

                    abs(a)
                }
            }

        assertThat(ggT(153, 81)).isEqualTo(9)
        assertThat(ggT(81, 153)).isEqualTo(9)
        assertThat(ggT(0, 153)).isEqualTo(153)
        assertThat(ggT(153, 0)).isEqualTo(153)
    }

    /**
     * Berechnung über ggT
     * siehe: https://de.wikipedia.org/wiki/Kleinstes_gemeinsames_Vielfaches#Berechnung_%C3%BCber_den_gr%C3%B6%C3%9Ften_gemeinsamen_Teiler_(ggT)
     * kgV(z1,z2) = (z1 * z2) / ggT(z1,z2)
     */
    @Test
    fun `kgV für zwei ganze Zahlen`() {
        fun ggT(z1: Int, z2: Int): Int =
            when {
                z1 == 0 -> abs(z2)
                z2 == 0 -> abs(z1)
                else -> {
                    var a = z1
                    var b = z2
                    do {
                        val r = a % b
                        a = b
                        b = r
                    } while (b != 0)

                    abs(a)
                }
            }

        fun kgV(z1: Int, z2: Int) = (z1 * z2) / ggT(z1, z2)

        assertThat(kgV(72, 120)).isEqualTo(360)
    }
}