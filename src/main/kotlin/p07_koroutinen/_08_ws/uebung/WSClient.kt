package p07_koroutinen._08_ws.uebung

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.websocket.WebSockets
import io.ktor.client.features.websocket.ws
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.time.delay
import mu.KotlinLogging
import java.time.Duration

@UseExperimental(KtorExperimentalAPI::class)
fun main() = runBlocking {
    val client = HttpClient(CIO) {
        install(WebSockets)
    }


    repeat(5) { clientId ->
        launch {

            val request: HttpRequestBuilder.() -> Unit = {
                header("client.id", "client $clientId")
            }
            client.ws(host = "127.0.0.1", port = 8080, path = "/ping", request = request) {
                val logger = KotlinLogging.logger("client")

                coroutineScope {
                    val sendJob = launch {
                        while (isActive) {
                            delay(Duration.ofSeconds(1))
                            outgoing.send(Frame.Text("ping"))
                        }
                    }

                    var count = 0
                    for (message in incoming) {
                        if (message is Frame.Text) {
                            logger.info(message.readText())
                            count++
                            if (count == 3) {
                                break
                            }
                        }
                    }

                    sendJob.cancel()
                }

            }

        }
    }
}