# Koroutinen

 * Melvin Conway 1963
 * Kooperative Übergabe des Kontrollflusses
 * Koroutinen sind *sequentiell* per Default

---

# Funktionen / Routinen
![](_routine.png)

---

# Koroutinen
![](_coroutine.png)

---

# Stackless vs Stackfull Koroutinen

* Stackless: Suspendierungen sind nur __direkt__ in Koroutinen möglich
* Stackfull: Suspendierungen sind __überall__ möglich
* Kotlin implementiert __stackless__ Koroutinen

---

# Continuations

```kotlin
suspend fun createCollage(query: String, count: Int): BufferedImage {
    val urls = requestImageUrls(query, count) //Label 0
    val images = mutableListOf<BufferedImage>() //Label 1
    for (index in 0 until urls.size) {
        val image = requestImageData(urls[index])
        images += image //Label 2
    }
    val newImage = combineImages(images)
    return newImage
}
```

![](_continuation.png)

---

# Kotlin Compiler

Ausgangscode: 

```kotlin
suspend fun createCollage(
    query: String, count: Int
): BufferedImage {
    ...
}
```

Compiler erzeugt daraus:

```kotlin
fun createCollage(
    query: String, count: Int,
    parentContinuation: Continuation<BufferedImage>
): Any // BufferedImage | COROUTINE_SUSPENDED {
    val cont = CoroutineImpl(parentContinuation) //Implements Continuation
    ...
}
```

![](_continuation-full.png)

---

##### Continuations-Stack

![](_continuation-list.png)

---

##### Einstieg und Absprung?

![](_coroutine-builder-lib.png)

---
# Integration mit asynchronen Libraries

```kotlin
suspendCoroutine<List<String>> { continuation ->
    JerseyClient.qwant("q=$query&count=$count").request().async()
        .get(object : InvocationCallback<String> {
            override fun completed(response: String) {
                val urls = JsonPath.read<List<String>>(response, "$..thumbnail")
                continuation.resume(urls)
            }

            override fun failed(throwable: Throwable) {
                continuation.resumeWithException(throwable)
            }
        })
}
```

