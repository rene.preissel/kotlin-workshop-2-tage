package p02_oo_klassen._08b.java;

public class QuotientMitRest {

    private GeldBetrag quotient;
    private GeldBetrag rest;

    public QuotientMitRest(GeldBetrag quotient, GeldBetrag rest) {
        this.quotient = quotient;
        this.rest = rest;
    }

    public GeldBetrag getQuotient() {
        return quotient;
    }

    public GeldBetrag getRest() {
        return rest;
    }
}
