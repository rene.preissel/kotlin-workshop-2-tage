package p02_oo_klassen

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.math.round

/**
 * ## Klassen in Kotlin
 *
 * - Besonderheiten bei der Deklaration
 * - Objekte (Instanzen) erzeugen.
 * - Primäre und sekundäre Konstruktoren
 * - Vererbung
 *
 */
class _05_Klasssen_und_Objekte {


    /**
     * ### Deklaration und Konstrukturaufruf
     *
     *
     *  * Deklarationsblock `{...}` ist optional.
     *
     *  * Geschachtelte Deklaration, z. B. innerhalb von Funktionen, ist
     *    auch mit benannten Klassen erlaubt.
     *
     *  * Kein `new` beim Konstruktoraufruf
     *    - Konstruktoren erkennt man daran, dass sie mit einem Großbuchstaben beginnen.
     *
     */
    @Test
    fun `01 Die kleinste aller Klassen`() {


        class FastNix


        val n = FastNix()


        assertThat(n.toString()).contains("FastNix")
        assertTrue(n is FastNix)
    }


    /**
     * ## Properties und Funktionen
     *
     *  * *Properties* als `val` oder `var`
     *
     *  * *Funktionen* (aka *Methoden*) als `fun`
     *
     *  * `public` ist Default und wird meist weggelassen.
     *
     *  * Sichtbarkeiten:
     *      - `private` - anders als in Java wirklich nur innerhalb der aktuellen Klasse
     *      - `protected` - genauso wie `private` und zusätzlich in Subklassen
     *      - `internal` - für jeden im selben Module (Compile-Einheit)
     *      - `public` - für alle
     *
     */
    @Test
    fun `02 Eine einfache Klasse`() {


        class Rechteck {

            var breite = 0
            var hoehe = 0

            fun berechneFlaeche() = breite * hoehe

        }


        val r = Rechteck()
        r.breite = 3
        r.hoehe = 4


        val flaeche = r.berechneFlaeche()

        assertThat(r.breite).isEqualTo(3)
        assertThat(r.hoehe).isEqualTo(4)
        assertThat(flaeche).isEqualTo(12)
    }


    /**
     * ## Primärer Konstruktur
     *
     * * Jede Klasse hat genau einen(!( *primären Konstruktor*
     *
     * * Dieser wird bei jeder Instanzerzeugung aufgerufen
     *
     * * Seine Parameter können im Deklarationsblock zur Initialisierung von
     *   Feldern genutzt werden.
     */
    @Test
    fun `03 Ein primärer Konstruktor`() {


        class Rechteck constructor(b: Int, h: Int) {

            val breite = b
            val hoehe = h

        }


        val r = Rechteck(3, 4)


        assertThat(r.breite).isEqualTo(3)
        assertThat(r.hoehe).isEqualTo(4)
    }


    /**
     * * Man darf das Schlüsselwort `constructor` beim primären constructor auch weglassen.
     *
     * * Machmal ist es erforderlich z. B. wenn man einen `private constructor`
     *   erstellen möchte.
     *
     * * Hat man nur einen Konstruktur, dann entspricht die Deklaration
     *   (hier `Rechteck(b: Int, h: Int)`) sehr schön der Instanzerzeugung,
     *   (hier `Rechtec(3,4)` ).
     */
    @Test
    fun `04 Man das Keyword constructor auch weglassen`() {


        class Rechteck(b: Int, h: Int) {
            val breite = b
            val hoehe = h
        }


        val r = Rechteck(3, 4)


        assertThat(r.breite).isEqualTo(3)
        assertThat(r.hoehe).isEqualTo(4)
    }

    /**
     * ## Felddeklarationen im primären Konstruktor
     *
     * Viele Konstruktoren tun nichts weiter, als ein paar Felder zu initialiseren.
     * Dafür gibt es eine Kurznotation.
     * Wenn man den Parametern des primären Konstruktors `val` oder
     * `var` vorangestellt, wird ein ex
     */
    @Test
    fun `05 Mit var oder val können Felder auch im primären Konstruktur deklariert werden`() {


        class Rechteck(val breite: Int, val hoehe: Int)

        val r = Rechteck(3, 4)

        assertThat(r.breite).isEqualTo(3)
        assertThat(r.hoehe).isEqualTo(4)
    }

    /**
     * ## Sekundäre Konstruktoren
     *
     * * Es gibt *immer* nur genau einen primären Konstruktor.
     *
     * * Man darf weitere Sekundäre Konstrukturen hinzufügen
     *
     *   - Grund:
     *     - weitere Konstruktoren für mehr Komfort
     *     - Kapselung des primären Konstruktors
     *
     *   - Mit `constructor(...)` im Deklarationsblock
     *
     *   - Die sekundären Konstruktoren müssen den primären Konstruktor oder einen anderen sekundären Konstruktor aufrufen (mit `: this(...)`)
     *
     *   - Ein Code-Block darf angegeben werden.
     */
    @Test
    fun `06 Mehrere Konstruktoren`() {

        class NumericString private constructor(val stringRepresentation: String) {

            constructor(ganzeZahl: Int) : this(ganzeZahl.toString())

            constructor(ganzeZahl: Double) : this(ganzeZahl.toString())

            constructor(p: Int, q: Int) : this("$p/$q") {
                if (q == 0)
                    throw IllegalArgumentException("Important: q must not be 0!")
            }
        }


        assertThat(NumericString(42).stringRepresentation).isEqualTo("42")
        assertThat(NumericString(47.11).stringRepresentation).isEqualTo("47.11")
        assertThat(NumericString(3, 4).stringRepresentation).isEqualTo("3/4")
        assertThatThrownBy { NumericString(3, 0) }
            .hasMessageContaining("must not be 0")
    }


    /**
     * ## `init`-Blöcke werden bei jeder Instanzerzeugung aufgerufen.
     *
     * Wenn man möchte, dass mit dem primären Konstruktor extra Code
     * ausgeführt wird, nimmt man einen `init`-Block.
     */
    @Test
    fun `07 Wenn vorhanden, wird mit dem primären Konstruktor auch ein init-Block ausgeführt`() {


        class Betrag(b: Double) {

            val betrag: Double

            init {
                betrag = round(b * 100.0) / 100.0
            }

        }

        assertThat(Betrag(47.11).betrag).isEqualTo(47.11)
        assertThat(Betrag(47.1134).betrag).isEqualTo(47.11)
        assertThat(Betrag(47.115).betrag).isEqualTo(47.12)
        assertThat(Betrag(47.1163).betrag).isEqualTo(47.12)
    }


    /**
     * ## Vererbung
     *
     * - Nur erlaubt, wenn Oberklasse als `open` deklariert ist.
     * - Nach dem `:` wird ein Konstruktur der Oberklasse aufgerufen.
     */
    @Test
    fun `08 Vererbung`() {

        open class Name(val vorname: String, val name: String)

        class Ansprache(val anrede: String, vorname: String, name: String) : Name(vorname, name)


        val a = Ansprache("Lady", "Ada", "Lovelace")
        assertThat(a.anrede).isEqualTo("Lady")
        assertThat(a.vorname).isEqualTo("Ada")
        assertThat(a.name).isEqualTo("Lovelace")
    }


    /**
     * ## Interfaces
     *
     * * Sehr ähnlich wie in Java
     * * `override` muss angegeben werden. (Gilt auch bei Vererbung)
     *
     */
    @Test
    fun `09 Interfaces`() {

        // Deklaration des Interfaces `HelloSayer` unterhalb dieser Funktion

        class Moin : HelloSayer {

            override fun sayHello() = println("Moin Moin!")

        }

        Moin().sayHello()
    }

    interface HelloSayer {
        fun sayHello()
    }


    /**
     * ## Objekte
     *
     * Soll es von einer Klasse nur ein Objekt geben (Singleton) kann man dieses gleich per `object` definieren
     * * Singleton-Objekte beginnen wie Klassen mit einem großen Buchstaben
     * * Keine Konstruktorparameter möglich, da das Objekt automatisch erzeugt wird (`init` ist möglich)
     * * Objekte können auch local angelegt werden: entspricht anonymer innerer Klasse
     */
    @Test
    fun `10 Singleton Objekte()`() {
        // Deklaration des Objekts `MoinMoin` unterhalb dieser Funktion

        MoinMoin.sayHello()

        // lokales Objekt welches ein Interface implementiert
        val localMoin = object : HelloSayer {
            override fun sayHello() = println("Moin Moin!")
        }

        localMoin.sayHello()
    }

    object MoinMoin : HelloSayer {
        override fun sayHello() = println("Moin Moin!")
    }


    /**
     * ## Kein `static` in Kotlin
     *
     * Kotlin kein kein `static` Keyword, wie Java.
     * Aber natürlich gibt es sinnvolle Alternativen
     *
     * In Kotlin müssen Funktionen nicht Teil einer Klasse
     * sein.
     *
     * Utility-Funktionen, Konstanten und globale Variablen (Bäh!)
     * könne einfach auf dem Top-Level deklariert werden.
     */
    @Test
    fun `11 Klassenunabhängige Methoden und Konstanten()`() {

        assertThat(

            square(3)        // Aus _05aMyUtils.kt

        ).isEqualTo(9)


        assertThat(

            A_SQUARE_NUMBER        // Aus _05aMyUtils.kt

        ).isEqualTo(49)

    }

    /**
     * ### Companion Objects
     *
     * Utility-Funktionen, Konstanten und globale Variablen
     * einer bestimmten Klasse zuordnen kann man sie
     * in einem `companion object` stecken.
     *
     * * Das Companion Objekt ist wird nach dem Laden der dazugehörigen Klasse initialisiert.
     *
     * * Felder und Funktionen können über den Namen der umgebenden Klasse
     *   adressiert werden (wie `static`'s in Java).
     */
    @Test
    fun `12 Ein Companion als Factory`() {


        // Anmerkung die Beispielklasse ist unterhalb definiert,
        // weil lokale Klassen in Funktionen keine Companions haben dürfen.


        val a = NummeriertesObjekt.next()
        val b = NummeriertesObjekt.next()
        val c = NummeriertesObjekt.next()
        val d = NummeriertesObjekt.DIESE_ZAHL_SCHON_WIEDER


        assertThat(a.nr).isEqualTo(0)
        assertThat(b.nr).isEqualTo(1)
        assertThat(c.nr).isEqualTo(2)
        assertThat(d).isEqualTo(42)
    }

    class NummeriertesObjekt private constructor(val nr: Int) {

        companion object {
            var nextNr = 0

            fun next() = NummeriertesObjekt(nextNr++)

            val DIESE_ZAHL_SCHON_WIEDER = 42
        }
    }


    /**
     * ### Enums
     *
     * Anders als in Java heißt es hier `enum class` an Stelle von `enum`.
     *
     */
    @Test
    fun `13 Ein einfaches Enum`() {

        // Deklaration unten

        assertThat(
            Richtung.LINKS
        ).isNotEqualTo(
            Richtung.RECHTS
        )

        assertThat(
            Richtung.values().size
        ).isEqualTo(2)
    }

    enum class Richtung { LINKS, RECHTS }

}