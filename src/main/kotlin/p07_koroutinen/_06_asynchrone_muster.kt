@file:UseExperimental(ExperimentalCoroutinesApi::class)

package p07_koroutinen

import com.jayway.jsonpath.JsonPath
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.selects.select
import kotlinx.coroutines.selects.selectUnbiased
import kotlinx.coroutines.time.delay
import kotlinx.coroutines.time.onTimeout
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import java.time.Duration
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * # Asynchrone Muster / Konzepte in Kotlin
 * * Sequential by default
 * * Asynchronous explicitly
 * * Libraries not language
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class _06_asynchrone_muster {


    @Test
    fun `01 Sequential by default`() = runBlocking {

        suspend fun createCollage(query: String, count: Int): BufferedImage {
            val urls = requestImageUrls(query, count)
            val images = urls.map { requestImageData(it) }
            val newImage = combineImages(images)
            return newImage
        }

        val image = createCollage("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `02 Sequential by default`() = runBlocking {

        suspend fun createCollageAsyncAwait(query: String, count: Int): BufferedImage = coroutineScope {
            val urls = requestImageUrls(query, count)
            val deferredImages: List<Deferred<BufferedImage>> = urls.map {
                async {
                    requestImageData(it)
                }
            }

            val images = deferredImages.map { it.await() }
//            val images: List<BufferedImage> = deferredImages.awaitAll()

            combineImages(images)
        }

        val image = createCollageAsyncAwait("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `02 Select um auf erstes Ereignis zu warten`() = runBlocking {

        val DEFAULT_IMAGE: BufferedImage = BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR)

        suspend fun loadFastestImage(query: String, count: Int): BufferedImage = coroutineScope {
            val urls = requestImageUrls(query, count)
            val deferredImages = urls.map {
                async {
                    requestImageData(it)
                }
            }
            val image: BufferedImage = select {
                for (deferredImage in deferredImages) {
                    deferredImage.onAwait { image ->
                        image
                    }
                }

                onTimeout(Duration.ofSeconds(5)) {
                    DEFAULT_IMAGE
                }
            }
            deferredImages.forEach { it.cancel() }
            image
        }

        val image = loadFastestImage("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `03 CSP Channels`() = runBlocking {

        suspend fun retrieveImages(query: String, channel: SendChannel<BufferedImage>) {
            while (isActive) {
                try {
                    val url = requestImageUrl(query)
                    val image = requestImageData(url)
                    channel.send(image)
                    delay(Duration.ofSeconds(2))
                } catch (exc: Exception) {
                    delay(Duration.ofSeconds(2))
                }
            }
        }

        suspend fun createCollage(channel: ReceiveChannel<BufferedImage>, count: Int) {
            var imageId = 0
            while (isActive) {
                val images = (1..count).map {
                    channel.receive()
                }
                val collage = combineImages(images)
                ImageIO.write(collage, "png", FileOutputStream("image-${imageId++}.png"))
            }
        }

        val channel = Channel<BufferedImage>()
        val dogsJob = launch(Dispatchers.Unconfined) {
            retrieveImages("dogs", channel)
        }

        val catsJob = launch(Dispatchers.Unconfined) {
            retrieveImages("cats", channel)
        }

        val collageJob = launch(Dispatchers.Unconfined) {
            createCollage(channel, 4)
        }
        delay(Duration.ofMinutes(1))

        dogsJob.cancel()
        catsJob.cancel()
        collageJob.cancel()
    }


    @Test
    fun `04 Producer und Consumer`() = runBlocking {
        suspend fun CoroutineScope.retrieveImages(query: String): ReceiveChannel<BufferedImage> = produce {
            while (isActive) {
                try {
                    val url = requestImageUrl(query)
                    val image = requestImageData(url)
                    send(image)
                    delay(Duration.ofSeconds(2))
                } catch (exc: Exception) {
                    delay(Duration.ofSeconds(1))
                }
            }
        }

        suspend fun createCollage(count: Int, vararg channels: ReceiveChannel<BufferedImage>): BufferedImage {
            val images = (1..count).map {
                selectUnbiased<BufferedImage> {
                    channels.forEach { channel ->
                        channel.onReceive { it }
                    }
                }
            }
            return combineImages(images)
        }

        val dogsChannel = retrieveImages("dogs")
        val catsChannel = retrieveImages("cats")

        val collageJob = launch(Dispatchers.Unconfined) {
            var imageId = 0
            while (isActive) {
                val collage = createCollage(4, catsChannel, dogsChannel)
                ImageIO.write(collage, "png", FileOutputStream("image-${imageId++}.png"))
            }
        }
        delay(Duration.ofMinutes(2))

        dogsChannel.cancel()
        catsChannel.cancel()
        collageJob.cancel()
    }

    @Test
    fun `05 Broadcast Channels`() = runBlocking {
        val channel = Channel<String>()

        repeat(5) { index ->
            launch {
                for (msg in channel) {
                    println("Channel empfangen $index: $msg")
                }
            }
        }

        channel.send("A")
        channel.send("B")
        channel.send("C")
        channel.cancel()

        val broadcastChannel = BroadcastChannel<String>(1)

        repeat(5) { index ->
            launch {
                for (msg in broadcastChannel.openSubscription()) {
                    println("BroadcastChannel empfangen $index: $msg")
                }
            }
        }

        delay(Duration.ofSeconds(1))
        broadcastChannel.send("A")
        broadcastChannel.send("B")
        broadcastChannel.send("C")
        broadcastChannel.cancel()

        Unit
    }

    suspend fun requestImageUrl(query: String) = suspendCoroutine<String> { cont ->
        JerseyClient.qwant("q=$query&count=200")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    val url = urls.shuffled().firstOrNull() ?: return failed(IllegalStateException("No image found"))
                    cont.resume(url)
                }

                override fun failed(throwable: Throwable) {
                    cont.resumeWithException(throwable)
                }
            })
    }

    suspend fun requestImageUrls(
        query: String,
        count: Int = 20
    ) = suspendCoroutine<List<String>> { continuation ->
        JerseyClient.qwant("q=$query&count=$count").request().async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    continuation.resume(urls)
                }

                override fun failed(throwable: Throwable) {
                    continuation.resumeWithException(throwable)
                }
            })
    }

    suspend fun requestImageData(imageUrl: String) = suspendCoroutine<BufferedImage> { cont ->
        JerseyClient.url(imageUrl)
            .request(MediaType.APPLICATION_OCTET_STREAM)
            .async()
            .get(object : InvocationCallback<InputStream> {
                override fun completed(response: InputStream) {
                    val image = ImageIO.read(response)
                    cont.resume(image)
                }

                override fun failed(throwable: Throwable) {
                    cont.resumeWithException(throwable)
                }
            })
    }

    @AfterAll
    fun closeJerseyClient() {
        JerseyClient.close()
    }

}


