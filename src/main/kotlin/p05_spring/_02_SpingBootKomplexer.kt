@file:Suppress("PackageDirectoryMismatch")

package p05_spring.komplex


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class Person(
    val name: String,

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    val id: Int = 0
)

@Transactional
interface PersonRepository : JpaRepository<Person, Int>

@RestController
class PersonController(val repo: PersonRepository) {
    //Name der Variablen wird aus dem Parameternamen extrahiert
    @PostMapping("/person")
    fun savePerson(@RequestParam name: String): String {
        val p = repo.save(Person(name))
        return "New person with id: ${p.id}"
    }

    @GetMapping("/person/{id}")
    @ResponseBody
    fun loadPerson(@PathVariable id: Int): ResponseEntity<Person> =
        repo.findById(id).map { ResponseEntity.ok(it) }.orElse(ResponseEntity.notFound().build())
}

@SpringBootApplication
@EntityScan
@EnableTransactionManagement
@EnableJpaRepositories
class SpringDataApplication

fun main(args: Array<String>) {
    runApplication<SpringDataApplication>(*args)
}
