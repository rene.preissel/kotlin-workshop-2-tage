@file:Suppress("PackageDirectoryMismatch")

package p05_spring.beans


import org.springframework.beans.factory.getBean
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans

class WorldService {
    fun sayWorld() = "World"
}

class HelloService(val worldService: WorldService) {
    fun sayHelloWorld() = "Hello ${worldService.sayWorld()}"
}


fun main() {

    val beanDefinitions = beans {
        //bean() ist reified um den Typ über den generischen Parameter zu ermitteln
        bean<WorldService>()

        bean("helloWorld") {

            //ref() ist inline reified und findet automatisch das richtige Bean
            HelloService(ref())

        }
    }

    val context = GenericApplicationContext().apply {
        beanDefinitions.initialize(this) // die Definitions werden auf den Context angewendet
        refresh()
    }

    //Typ wird durch Reified ermittelt
    val service: HelloService = context.getBean()
    println(service.sayHelloWorld())

}
