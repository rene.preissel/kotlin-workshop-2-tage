package p07_koroutinen

import java.lang.management.ManagementFactory
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import kotlin.concurrent.thread

/*
  AUFGABE:
  Erzeuge eine Kopie dieser Datei und
  konvertiere  den folgenden Code von Threads zu Koroutinen
  Tipps:
  * Die gesamte main-Funktion muss mit runBlocking umschlossen werden
  * Aus thread wird launch
  * Aus submit wird async
  * Aus sleep wird delay
  * Aus Future.get wird Deferred.await
  Führe die neue Main-Funktion aus und erkläre den Unterschied zur Thread-Variante
  Wieviele Threads werden neu erstellt?
 */
fun main() {
    val threadMX= ManagementFactory.getThreadMXBean()
    thread {
        while (true) {
            Thread.sleep(50)
            println("Threads: ${threadMX.threadCount}")
        }
    }

    val executor = Executors.newCachedThreadPool()
    val helloList = (1..100).map { index ->
        executor.submit(Callable {
            Thread.sleep(100L + index)
            if (index == 42) {
                throw IllegalStateException()
            }
            println("Hello from $index")
            index
        })
    }

    val result = helloList.map { it.get() }.joinToString()
    println(result)
}