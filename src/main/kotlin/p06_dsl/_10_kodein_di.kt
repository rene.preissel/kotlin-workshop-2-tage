package p06_dsl

import org.h2.jdbcx.JdbcDataSource
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.generic.with
import org.kodein.di.newInstance
import javax.sql.DataSource

/**
 * ## Kodein - Dependency Injection
 * https://github.com/Kodein-Framework/Kodein-DI ist ein Dependency Injection Container und
 * nutzt Infix-Funktionen um lesbareren Code zu erzeugen
 */
class _10_kodein_di {

    /**
     * constant - definiert eine Konstante
     * bind - registriert ein Bean
     * with - verknüpft die Registrierung mit einem Objekt
     * from - so ähnlich wie with, nur wird der Typ von bind durch das Objekt bestimmt
     */
    @Test
    fun `01 Infix für die Dependencies-Definition`() {
        class DatabaseService(val dataSource: DataSource, val dbUrl: String)

        val kodein = Kodein {
            constant("dburl") with "jdbc:h2:mem:singleton"
            bind<DataSource>() with singleton { JdbcDataSource().apply { setURL(instance("dburl")) } }
            bind() from singleton { DatabaseService(instance(), instance("dburl")) }
        }

        //instance nutzt reified Typparameter
        val databaseService = kodein.direct.instance<DatabaseService>()
        assertEquals("jdbc:h2:mem:singleton", databaseService.dbUrl)
        assertNotNull(databaseService.dbUrl)
    }

    @Test
    fun `02 Delegation Properties sind auch möglich für das Inject`() {
        class DatabaseService(override val kodein: Kodein) : KodeinAware {
            val dataSource: DataSource by instance()
            val dbUrl: String by instance("dburl")
        }

        val kodein = Kodein {
            constant("dburl") with "jdbc:h2:mem:singleton"
            bind<DataSource>() with singleton { JdbcDataSource().apply { setURL(instance("dburl")) } }
        }

        val databaseService = DatabaseService(kodein)
        assertEquals("jdbc:h2:mem:singleton", databaseService.dbUrl)
        assertNotNull(databaseService.dbUrl)
    }

    @Test
    fun `03 Delegation Properties können auch direkt zum Erzeugen benutzt werden`() {
        class DatabaseService(val dataSource: DataSource, val dbUrl: String)

        val kodein = Kodein {
            constant("dburl") with "jdbc:h2:mem:singleton"
            bind<DataSource>() with singleton { JdbcDataSource().apply { setURL(instance("dburl")) } }
        }

        val databaseService by kodein.newInstance {
            DatabaseService(instance(), instance("dburl"))
        }
        assertEquals("jdbc:h2:mem:singleton", databaseService.dbUrl)
        assertNotNull(databaseService.dbUrl)
    }

}


