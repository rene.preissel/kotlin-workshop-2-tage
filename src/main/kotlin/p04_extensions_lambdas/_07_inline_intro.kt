package p04_extensions_lambdas

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * ##  Inline Funktionen
 * * Bei Inline-Funktionen wird der Funktions-Rumpf an die Aufrufstelle kopiert. Sinnvoll bei eigenen Kontrollstrukturen mit Lambdas:
 */
class _07_inline_intro {
    fun timesNotInline(times: Int, action: (Int) -> Unit) {
        for (index in 0 until times) {
            action(index)
        }
    }

    inline fun timesInline(times: Int, action: (Int) -> Unit) {
        for (index in 0 until times) {
            action(index)
        }
    }

    @Test
    fun `01 Times inline and not inline`() {
        timesNotInline(5) { index ->
            println(index)
        }

        timesInline(5) { index ->
            println(index)
        }
    }

    @Test
    fun `02 Return in Inline Funktionen verlässt umschliessende Funktion`() {
        fun doSomething(): Boolean {

            timesInline(5) { index ->
                if (index == 2) {
                    return false
                }
                println(index)
            }

            return true
        }

        val ret = doSomething()
        assertEquals(false, ret)
    }

    @Test
    fun `03 Return mit Label ermöglicht explizit das Verlassen des aktuellen Lambdas`() {
        fun doSomething(): Boolean {

            timesInline(5) { index ->
                if (index == 2) {
                    //ACHTUNG: es wird weiterhin die nächste Schleife durchlaufen
                    return@timesInline
                }
                println(index)
            }

            return true
        }

        val ret = doSomething()
        assertEquals(true, ret)
    }

}


