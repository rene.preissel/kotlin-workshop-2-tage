package p04_extensions_lambdas

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.math.PI

/**
 * ## Extensions Properties
 */
class _02_extensions_properties {
    @Test
    fun `01 Einfache Extensions Funktion`() {
        val circle = Circle(10.0)
        val rect = Rectangle(5.0, 4.0)

        assertEquals(PI * 100, circle.area)
        assertEquals(20.0, rect.area)
    }
}

class Circle(val radius: Double)
class Rectangle(val width: Double, val height: Double)

val Circle.area: Double
    get() = PI * radius * radius

val Rectangle.area: Double
    get() = width * height

