package p07_koroutinen

import com.jayway.jsonpath.JsonPath
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class _01d_beispiel_mit_koroutinen {


    @Test
    fun `01 Ein Bild laden mit Koroutinen`() = runBlocking {

        suspend fun loadOneImage(query: String): BufferedImage {
            val url = requestImageUrl(query)
            val image = requestImageData(url)
            return image
        }

        val image = loadOneImage("dogs")
        ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
        println("${image.width}x${image.height}")
    }

    @Test
    fun `02 Mehrere Bilder laden mit Koroutinen`() = runBlocking{

        suspend fun createCollage(query: String, count: Int): BufferedImage {
            val urls = requestImageUrls(query, count)
            val images = urls.map { requestImageData(it) }
            val newImage = combineImages(images)
            return newImage
        }

        val image = createCollage("dogs", 20)
        ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
        println("${image.width}x${image.height}")
    }


    suspend fun requestImageUrl(query: String) = suspendCoroutine<String> { cont ->
        JerseyClient.qwant("q=$query&count=200")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    val url = urls.shuffled().firstOrNull() ?: return failed(IllegalStateException("No image found"))
                    cont.resume(url)
                }

                override fun failed(throwable: Throwable) {
                    cont.resumeWithException(throwable)
                }
            })
    }

    suspend fun requestImageUrls(
        query: String,
        count: Int = 20
    ) = suspendCoroutine<List<String>> { continuation ->
        JerseyClient.qwant("q=$query&count=$count").request().async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    continuation.resume(urls)
                }

                override fun failed(throwable: Throwable) {
                    continuation.resumeWithException(throwable)
                }
            })
    }

    suspend fun requestImageData(imageUrl: String) = suspendCoroutine<BufferedImage> { cont ->
        JerseyClient.url(imageUrl)
            .request(MediaType.APPLICATION_OCTET_STREAM)
            .async()
            .get(object : InvocationCallback<InputStream> {
                override fun completed(response: InputStream) {
                    val image = ImageIO.read(response)
                    cont.resume(image)
                }

                override fun failed(throwable: Throwable) {
                    cont.resumeWithException(throwable)
                }
            })
    }

    @AfterAll
    fun closeJerseyClient() {
        JerseyClient.close()
    }
}


