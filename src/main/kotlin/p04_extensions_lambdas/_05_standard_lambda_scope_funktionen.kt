package p04_extensions_lambdas

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.io.File

/**
 * ## Hilfreiche Standardfunktionen
 * * die folgenden Funktionen erlauben idiomatischen Kotlin-Code
 * * sie vermeiden häufig Hilfsvariablen und komplexe Ausdrücke
 *
 * Für die Auswahl siehe:
 * ['Mastering Kotlin standard functions: run, with, let, also and apply'](https://medium.com/@elye.project/mastering-kotlin-standard-functions-run-with-let-also-and-apply-9cd334b0ef84)
 */
class _05_standard_lambda_scope_funktionen {
    data class Address(val city: String, val zip: String)
    class Person(val firstName: String, val surname: String, val email: String, val address: Address? = null)

    val person1 = Person("Rene", "Preissel", "rp@etosquare.de", Address("Hamburg", "22391"))
    val person2 = Person("Anke", "Preissel", "anke@etosquare.de")


    /**
     * `with` kann direkt als Body-Block benutzt werden um den Zugriff auf einen Parameter zu vereinfachen
     */
    @Test
    fun `01 Nochmal with`() {

        fun createAddressLabel(person: Person): String = with(person) {
            """
            $firstName $surname
            ${address?.zip} ${address?.city}
            """.trimIndent()
        }

        println(createAddressLabel(person1))
    }

    /**
     * `let` erlaubt es den Receiver umzuwandeln, genauso wie `map` bei Collections
     */
    @Test
    fun `02 let verhält sich für normale Variablen wie map für Collections`() {

        val personList = listOf(person1, person2)
        val fullnameList = personList.map { "${it.firstName} ${it.surname}" }
        assertEquals("Rene Preissel", fullnameList[0])
        assertEquals("Anke Preissel", fullnameList[1])

        //Mit let geht das auch für ein Objekt
        val fullname = person1.let { "${it.firstName} ${it.surname}" }
        assertEquals("Rene Preissel", fullname)
    }

    @Test
    fun `03 let zusammen mit dem Fragezeichen-Operator ist noch mächtiger`() {
        val addressString1 = person1.address?.let { "${it.zip} ${it.city}" }
        assertEquals("22391 Hamburg", addressString1)

        val addressString2 = person2.address?.let { "${it.zip} ${it.city}" }
        assertNull(addressString2)

        val addressString3 = person2.address?.let { "${it.zip} ${it.city}" } ?: "unbekannt"
        assertEquals("unbekannt", addressString3)
    }

    /**
     * `run` ist wie `with`, nur definiert als Extension Funktion
     * * wenn der Receiver allerdings `null` sein kann, ist `run` idiomatischer
     */
    @Test
    fun `04 run verhält sich wie with`() {
        val addressLabel1 = with(person1) {
            """
            $firstName $surname
            ${address?.zip} ${address?.city}
            """.trimIndent()
        }

        val addressLabel2 = person1.run {
            """
            $firstName $surname
            ${address?.zip} ${address?.city}
            """.trimIndent()
        }

        assertEquals(addressLabel1, addressLabel2)

        //Bei Null-Werten ist run idiomatischer als with

        val addressString = person1.address?.run { "${zip} ${city}" }
        assertEquals("22391 Hamburg", addressString)

    }

    /**
     * 'apply' wird benutzt um die Initialisierung zu vereinfachen, wenn kein passender Konstruktor vorhanden ist
     * Ansonsten ist `apply` fast so wie `run` und `with`, nur das der Receiver zurückgegeben wird.
     */
    @Test
    fun `05 apply vereinfacht die Initialisierung`() {
        val weekdayMap = HashMap<String, String>().apply {
            put("Mo", "Monday")
            put("Tu", "Tuesday")
        }
        assertTrue(weekdayMap.containsKey("Mo"))
        assertTrue(weekdayMap.containsKey("Tu"))
    }

    /**
     * 'also' ist wie `apply` nur wird dabei der `this`-Zeiger nicht redefiniert, sondern ein normaler Parameter übergeben
     */
    @Test
    fun `06 also vereinfacht die Initialisierung auch`() {
        val weekdayMap = HashMap<String, String>().also { map ->
            map.put("Mo", "Monday")
            map.put("Tu", "Tuesday")
        }
        assertTrue(weekdayMap.containsKey("Mo"))
        assertTrue(weekdayMap.containsKey("Tu"))
    }

    /**
     * 'takeIf' in Kombination mit `?.let`/ `?.run` erlaubt das kompakte Schreiben von Bedingungen
     */
    @Test
    fun `07 takeIf ersetzt ein normales if`() {

        val onlyAddressesInHamburgString1 = if (person1.address?.zip?.startsWith("22") ?: false) {
            person1.address?.run { "$zip $city" }
        } else {
            "not in hamburg"
        }

        assertEquals("22391 Hamburg", onlyAddressesInHamburgString1)

        val onlyAddressesInHamburgString2 = person1.address
            ?.takeIf { it.zip.startsWith("22") }
            ?.run { "$zip $city" } ?: "not in hamburg"

        assertEquals("22391 Hamburg", onlyAddressesInHamburgString2)
    }

    /**
     * 'use' ist der Ersatz für `try-with-resources` aus Java
     * * `use` ist eine Extension-Funktion für ein `Closable`
     */
    @Test
    fun `08 use schliest Resourcen nach der Benutzung`() {

        File("address.txt").writer().use {
            it.write(person1.address?.run { "$zip $city" } ?: "unbekannt")
        }
    }


}


