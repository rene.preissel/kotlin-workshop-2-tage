package p04_extensions_lambdas;

import kotlin.collections.CollectionsKt;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static p04_extensions_lambdas.util._01b_UtilsKt.toURL;

public class _01a_extensions_from_java {
    @Test
    void extensionsFromJava() throws MalformedURLException {
        URL url = toURL("http://localhost");
        assertEquals(new URL("http://localhost"), url);

        List<Integer> ints = Arrays.asList(1, 2, 3);
        int sum = CollectionsKt.sumOfInt(ints);
        assertEquals(6, sum);
    }
}
