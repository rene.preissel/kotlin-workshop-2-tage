
# Warum Entwickler Kotlin lieben?
* [Umfrage März 2018](https://pusher.com/state-of-kotlin?section=dev-favorite)
    * 2744 Beteiligte
    
    ![](_kotlin_growth.png)
    
    ![](_kotlin_einsatz.png)
    
    ![](_kotlin_favorites.png )

# Weitere Umfrage  
* [Jetbrains Anfang 2018](https://www.jetbrains.com/research/devecosystem-2018/kotlin/)
    
    ![](_survey_type.png)

    ![](_survey_version.png)
