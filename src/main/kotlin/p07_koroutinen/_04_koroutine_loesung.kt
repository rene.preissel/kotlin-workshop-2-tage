package p07_koroutinen

import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.lang.management.ManagementFactory

fun main() = runBlocking {
    val threadMX= ManagementFactory.getThreadMXBean()
    launch {
        while (true) {
            delay(50)
            println("Threads: ${threadMX.threadCount}")
        }
    }

    val helloList = (1..100).map { index ->
        async {
            delay(100L + index)
            if (index == 42) {
                throw IllegalStateException()
            }
            println("Hello from $index")
            index
        }
    }

    val result = helloList.map { it.await() }.joinToString()
    println(result)

}