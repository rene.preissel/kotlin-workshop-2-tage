@file:Suppress("PackageDirectoryMismatch")

package p05_spring.routing


import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.support.beans
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Flux

class WorldService {
    fun sayWorld() = "World"
}

class HelloService(val worldService: WorldService) {
    fun sayHelloWorld() = "Hello ${worldService.sayWorld()}"
}

@SpringBootApplication
class WebfluxApplicationDSL

fun main(args: Array<String>) {

    val beanDefinitions = beans {
        bean<WorldService>()
        bean("helloWorld") {
            HelloService(ref())
        }
        bean {
            val helloService = ref<HelloService>()
            router {

                GET("/hello") {
                    ServerResponse.ok().syncBody(helloService.sayHelloWorld())
                }

                GET("/hello/{count}") {
                    val count = it.pathVariable("count").toInt()
                    val result = (1..count).map { helloService.sayHelloWorld() }.joinToString()
                    ServerResponse.ok().syncBody(result)
                }

                GET("/hello-reactive/{count}") {
                    val count = it.pathVariable("count").toInt()
                    val flux = Flux.create<String> {sink ->
                        repeat(count) {
                            sink.next(helloService.sayHelloWorld())
                        }
                        sink.complete()
                    }

                    val mono = flux.collectList().map { it.joinToString() }
                    ServerResponse.ok().body(mono)
                }
            }
        }
    }

    runApplication<WebfluxApplicationDSL>(*args) {
        addInitializers(beanDefinitions)
    }
}
