@file:Suppress("PackageDirectoryMismatch") @file:JvmName("KtorExample2Kt")

package p06_dsl.intro

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.body
import kotlinx.html.h1

/**
 *  Ktor ist ein asynchrones Server-Framework um Webanwendungen bzw. APIs umzusetzen.
 *  Ktor nutzt Extension-Funktionen anstelle von Vererbung.
 */
fun Application.main() {
    routing {
        get("/hello1") {
            call.respondTextWriter {
                appendln("Hello")
            }
        }

        //Zugriff auf URL Parameter
        get("/hello2/{name}") {
            val name = call.parameters["name"]
            if (name == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                call.respondTextWriter {
                    appendln("Hello $name")
                }
            }
        }

        //Integration mit Kotlinx.html im eigenem Modul
        get("/hello3/{name}") {
            val name = call.parameters["name"] ?: throw IllegalStateException("name missing")
            call.respondHtml {
                body {
                    h1 {
                        text("Hallo $name")
                    }
                }
            }
        }
    }

}


fun main() {
    val server = embeddedServer(
        Netty, port = 8080, module = Application::main
    )
    server.start(wait = true)
}