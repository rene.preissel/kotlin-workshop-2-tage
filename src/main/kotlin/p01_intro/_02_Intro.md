# Google Trends

   * [Kotlin](https://trends.google.de/trends/explore?date=2015-11-01%202018-12-01&q=/m/0_lcrx4)
   * [Kotlin vs Scala](https://trends.google.de/trends/explore?date=2015-11-01%202018-12-01&q=/m/0_lcrx4,/m/091hdj)
      

# Kotlin Entstehung
* Kotlin ist eine Insel - vor Sankt Petersburg 
* JetBrains stellt die Hauptentwickler der Sprache 


* __2011 Erste veröffentlichte Version__ 
* 2012 Unter Apache-2-Lizenz gestellt
* 2012 KotlinJS wird vorgestellt
* __2016 Version 1.0 veröffentlicht__
* 2017 Kotlin Native wird vorgestellt
* __2017 Kotlin wird offizielle Sprache für Android__
* 2018 Kotlin 1.3 mit offiziellen Support für Koroutinen



# Design Prinzipien Kotlin
* Interoparabilität mit Java 
* Einfachheit und Lesbarkeit
* Gute Unterstützung von DSLs
* Schneller Compiler 
* Pragramatische Lösungen vor akademischer Vollständigkeit
* Unterstützung von objektorientierter, prozeduraler und funktionaler Programmierung

   
# Erster Eindruck
   * [**Java Klasse**](_02a_Person.java)
   * [**Kotlin Klasse**](_02b_Person.kt)
