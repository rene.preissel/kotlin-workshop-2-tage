# Software Voraussetzungen
 
* [Java 8 - 11](http://jdk.java.net/11/)
* [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/download/)
* [Aktuelles Kotlin Plugin >= 1.3.x](https://plugins.jetbrains.com/plugin/6954-kotlin)
* [Markdown Plugin](https://plugins.jetbrains.com/plugin/7793-markdown-support)


# Beispiele und Übungen für den Workshop

* https://gitlab.com/rene.preissel/kotlin-workshop-2-tage

# Agenda

1. Einstieg
2. Grundlegende Syntax und Unterschiede zu Java
3. Grundlegende Datentypen und funktionale Programmierung
4. Erweiterte Sprach-Features von Kotlin
5. Kotlin und Spring    
6. Verschiedenste DSLs und weitere KotlinSprach-Features    
7. Koroutinen in Kotlin
8. Ausblick und Zeit für Experimente

# IntelliJ IDEA Keymap

* https://resources.jetbrains.com/storage/products/intellij-idea/docs/IntelliJIDEA_ReferenceCard.pdf

# Kotlin Cheat Sheet

Aus Copyright-Gründen hier nur die Links:

* https://devhints.io/kotlin
* https://kotlinexpertise.com/wp-content/uploads/2018/05/dzone_refcard_kotlin.pdf
* https://koenig-media.raywenderlich.com/uploads/2018/08/RW_Kotlin_Cheatsheet_1_0.pdf
