package p06_dsl

import org.junit.jupiter.api.Test

/**
 * ## DSLMarker ermöglicht typsichere Builder ohne das Ebenen übersprungen werden
 */
class _02_DSLMarker {


    @Test
    fun `01 Probleme ohne Einsatz von DSLMarker`() {

        class Inner

        class Outer {
            fun inner(block: Inner.() -> Unit) {}
        }

        fun outer(block: Outer.() -> Unit) {}


        outer {

            inner {
                //ERROR: Hier wird die Funktion beim Outer-This aufgerufen
                inner {

                }
            }
        }
    }

    @DslMarker
    @Target(AnnotationTarget.CLASS, AnnotationTarget.TYPEALIAS, AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
    annotation class MyDSL

    @Test
    fun `02 DSLMarker nimmt immer nur den innersten This-Zeiger implizit`() {

        @MyDSL
        class Inner

        @MyDSL
        class Outer {
            fun inner(block: Inner.() -> Unit) {}
        }

        fun outer(block: Outer.() -> Unit) {}


        outer {

            inner {
                // Jetzt gibt es einen Compile-Error
//              inner {
//
//              }

                // Explizit geht es weiterhin
                this@outer.inner {

                }
            }
        }
    }
}


