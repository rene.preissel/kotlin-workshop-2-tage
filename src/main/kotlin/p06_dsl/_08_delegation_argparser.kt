package p06_dsl

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

/**
 * ## ArgParser als Beispiel für _Delegated Properties_
 * * https://github.com/xenomachina/kotlin-argparser ist eine kleine Bibliothek um Kommandozeilenparameter zu parsen
 */
class _08_delegation_argparser {

    class GitCommitArgs(parser: ArgParser) {
        val amend by parser.flagging("amend commit")
        val all by parser.flagging("-a", "--all", help = "automatically stage files")
        val author by parser.storing("author").default("")
        val files by parser.positionalList("files", sizeRange = 0..Int.MAX_VALUE)
    }

    @Test
    fun `01 Delegation Properties können die Grundlage einer DSL sein`() {
        val args = ArgParser(arrayOf("-a", "--author", "Rene", "file.txt")).parseInto(::GitCommitArgs)


        assertAll(
            Executable { assertTrue(args.all) },
            Executable { assertFalse(args.amend) },
            Executable { assertEquals("Rene", args.author) },
            Executable { assertEquals(1, args.files.size) },
            Executable { assertEquals("file.txt", args.files[0]) }
        )
    }

    @Test
    fun `02 Automatische Generierung von Hilfe`() {
        mainBody {
            ArgParser(arrayOf("-h")).parseInto(::GitCommitArgs)
        }
    }
}


