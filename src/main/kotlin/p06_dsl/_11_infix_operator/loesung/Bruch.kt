package p06_dsl._11_infix_operator.loesung

import java.lang.Math.abs
import java.math.BigDecimal

fun ggT(z1: Int, z2: Int): Int =
    when {
        z1 == 0 -> Math.abs(z2)
        z2 == 0 -> Math.abs(z1)
        else -> {
            var a = z1
            var b = z2
            do {
                val r = a % b
                a = b
                b = r
            } while (b != 0)

            Math.abs(a)
        }
    }

infix fun Number.ueber(other: Number): Bruch = Bruch(this.toInt(), other.toInt())


data class Bruch private constructor(val zaehler: Int, val nenner: Int) : Comparable<Bruch> {

    val reziprok: Bruch by lazy {
        invoke(nenner, zaehler)
    }

    override operator fun compareTo(other: Bruch): Int =
        this.zaehler * other.nenner - this.nenner * other.zaehler

    operator fun plus(other: Bruch): Bruch =
        invoke(this.zaehler * other.nenner + this.nenner * other.zaehler, this.nenner * other.nenner)

    operator fun minus(other: Bruch): Bruch =
        invoke(this.zaehler * other.nenner - this.nenner * other.zaehler, this.nenner * other.nenner)

    operator fun times(other: Bruch): Bruch =
        invoke(this.zaehler * other.zaehler, this.nenner * other.nenner)

    companion object {
        val ZERO = Bruch(0,1)
        val ONE = Bruch(1,1)
        val TEN = Bruch(10,1)

        operator fun invoke(zaehler: Int, nenner: Int) =
            when {
                nenner < 1 ->
                    throw IllegalArgumentException("Nenner nicht positiv: $nenner")
                zaehler == 0 ->
                    Bruch(0, 1)
                else -> {
                    val ggT = ggT(abs(zaehler), nenner)
                    Bruch(zaehler / ggT, nenner / ggT)
                }
            }
    }
}

fun bruch(zaehler: Int, nenner: Int) =
    when {
        nenner < 1 ->
            throw IllegalArgumentException("Nenner nicht positiv: $nenner")
        zaehler == 0 ->
            Bruch(0, 1)
        else -> {
            val ggT = ggT(abs(zaehler), nenner)
            Bruch(zaehler / ggT, nenner / ggT)
        }
    }

fun String.toBruch(): Bruch {
    val bigDecimal = BigDecimal(this)
    val nenner = BigDecimal.TEN.pow(bigDecimal.scale())

    return Bruch((bigDecimal * nenner).toInt(), nenner.toInt())
}
