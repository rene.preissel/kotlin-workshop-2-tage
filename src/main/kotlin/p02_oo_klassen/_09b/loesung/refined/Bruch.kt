package p02_oo_klassen._09b.loesung.refined

import java.lang.Math.abs

class Bruch private constructor(z: Long,  n: Long) {
    val zaehler = z
    val nenner =n


    companion object {
        fun of(zaehler: Long, nenner: Long) =
                when {
                    nenner < 1 ->
                        throw IllegalArgumentException("Nenner nicht positiv: $nenner")
                    zaehler == 0L ->
                        Bruch(0, 1)
                    else -> {
                        val ggT = ggT(abs(zaehler), nenner)
                        Bruch(zaehler / ggT, nenner / ggT)
                    }
                }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Bruch

        if (zaehler != other.zaehler) return false
        if (nenner != other.nenner) return false

        return true
    }

    override fun hashCode(): Int {
        var result = zaehler.hashCode()
        result = 31 * result + nenner.hashCode()
        return result
    }


}




fun ggT(a: Long, b: Long): Long =
        when {
            a < 1 -> throw IllegalArgumentException("a out of range:$a")
            b < 1 -> throw IllegalArgumentException("b out of range:$a")
            a == b -> a
            a > b -> ggT(a - b, b)
            else -> ggT(b - a, a)
        }
