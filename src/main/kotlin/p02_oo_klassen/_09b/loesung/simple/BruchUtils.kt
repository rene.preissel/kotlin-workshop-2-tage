package p02_oo_klassen._09b.loesung.simple

object BruchUtils {

    fun ggT(a: Long, b: Long): Long {
        if (a < 1)
            throw IllegalArgumentException("a out of range:$a")

        if (b < 1)
            throw IllegalArgumentException("a out of range:$a")

        return if (a == b)
            a
        else if (a > b)
            ggT(a - b, b)
        else
            ggT(b - a, a)
    }

}
