@file:Suppress("PackageDirectoryMismatch")

package p05_spring.pure


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@Service
class WorldService {
    fun sayWorld() = "World"
}

/**
 * Konstruktor-Injection ist mit Kotlin sehr kompakt umzusetzen
 */
@Service
class HelloService(val worldService: WorldService) {
    fun sayHelloWorld() = "Hello ${worldService.sayWorld()}"
}


@RestController
class HelloWorldController() {

    /**
     * Field/Property-Injection
     * Mit `lateinit` sagen wir den Compiler das diese Varibale auf jeden Fall vor der ersten Verwendung initialisiert wird
     */
    @Autowired
    lateinit var service: HelloService

    @GetMapping("/hello")
    fun hello() = service.sayHelloWorld()
}

@SpringBootApplication
class WebApplication

fun main(args: Array<String>) {
    //Reified für Typparameter
    //*args : Spread-Operator wandelt ein Array in Varargs um
    //Lambda für Initialisierung
    runApplication<WebApplication>(*args) {
        setBannerMode(Banner.Mode.OFF)
    }
}
