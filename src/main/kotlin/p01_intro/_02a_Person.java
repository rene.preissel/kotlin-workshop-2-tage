package p01_intro;

class Person {
    private String name;
    private String vorname;
    private String geburtsname;

    public Person(String name, String vorname, String geburtsname) {
        this.name = name;
        this.vorname = vorname;
        this.geburtsname = geburtsname;
    }

    public Person(String name, String vorname) {
        this(name, vorname, null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getGeburtsname() {
        return geburtsname;
    }

    public void setGeburtsname(String geburtsname) {
        this.geburtsname = geburtsname;
    }
}
