@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_lambdas.w02_router_dsl.exercise

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import p05_spring.FeiertageService
import java.time.LocalDate

val PREFIXE_BUNDESLAENDER = listOf("/bayern" to "Bayern", "/hamburg" to "Hamburg", "" to null)

/*
AUFGABE
Level 1
Konvertiere die annotation-basierte feiertageFuerJahr()-Funktion zur funktionalen Router API.

Level 2
Erzeuge dynamisch anhand der PREFIXE_BUNDESLAENDER-Liste weitere Routings, d.h. `/bayern/2018`, `/hamburg/2018` und "/2018"
Für das Ermitteln der Daten bietet die feiertage()-Funktion einen zusätzlichen Parameter. feiertage(von,bis,bundesland)

Level 3 (Optional)
Schaue die Implementierung des Feiertagsservice an
* Was bedeutet der Datentyp:  Set<(LocalDate, LocalDate) -> Feiertag> ?
* was machen die Funktionen: `fixesDatum` und `relativZumOsterSonntag` ?

 */
@RestController
class FeiertagController(val feiertageService: FeiertageService) {

    @GetMapping("/today")
    fun today() = feiertageService.feiertage(LocalDate.now(), LocalDate.now()).isNotEmpty()

    @GetMapping("/{jahr}")
    fun feiertageFuerJahr(@PathVariable("jahr") jahr: Int) =
        feiertageService.feiertage(LocalDate.of(jahr, 1, 1), LocalDate.of(jahr, 12, 31))

}

@SpringBootApplication
class WebApplication {
    @Bean
    fun feiertagService() = FeiertageService()

}

fun main(args: Array<String>) {
    runApplication<WebApplication>(*args)
}
