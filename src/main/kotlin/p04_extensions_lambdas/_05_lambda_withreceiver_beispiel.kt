package p04_extensions_lambdas

import org.junit.jupiter.api.Test
import kotlin.math.PI




/**
 * ## Extensions Funktionen dienen zur Modularisierung / Erweiterbarkeit von Libraries/Framework
 */
class _05_lambda_withreceiver_beispiel {
    @Test
    fun `01 Lambdas with Receiver erlauben es eigene Kontrolltrukturen und DSLs zu definieren`() {

        class Circle(val radius: Double) : Loggable {
            val area: Double
                get() = withTracing("area") {
                    val r2 = radius * radius

                    // Diese Funktion wird über den this-Zeiger angeboten
                    traceStep("Radius Quadrat $r2")

                    PI * r2
                }
        }

        val circle = Circle(10.0)
        println(circle.area)


    }

}

//Neuen Context definieren, der später als This-Zeiger dienen soll
class TracingContext(val prefix: String, val loggable: Loggable) {
    fun traceStep(msg: String) = loggable.info("$prefix $msg")
}

//Lambda with receiver bekommt TracingContext als Receiver
fun <T> Loggable.withTracing(prefix: String, block: TracingContext.() -> T): T = try {
    info("$prefix - begin")
    val tracingContext = TracingContext(prefix, this)
    tracingContext.block()
} finally {
    info("$prefix - end")
}