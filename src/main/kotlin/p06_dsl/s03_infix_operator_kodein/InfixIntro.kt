package p06_dsl.s01_infix_operator_delegation.infixintro

class Circle(val x: Double, val y: Double, val radius: Double) {
    infix fun intersects(other: Circle): Boolean {
        val distanceX = this.x - other.x
        val distanceY = this.y - other.y
        val radiusSum = this.radius + other.radius
        return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum
    }
}

fun main(args: Array<String>) {
    val daysInfix = mapOf("Mo" to "Monday", "Tu" to "Tuesday")
    val days = mapOf("Mo".to("Monday"), "Tu".to("Tuesday"))

    val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
    val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)
    println(c1 intersects c2)
}