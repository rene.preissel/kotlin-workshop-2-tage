package p07_koroutinen

import com.jayway.jsonpath.JsonPath
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.awt.image.BufferedImage
import java.io.FileOutputStream
import java.io.InputStream
import java.util.concurrent.CompletableFuture
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class _01c_beispiel_mit_futures {


    @Test
    fun `01 Ein Bild laden mit Futures`() {

        fun loadOneImage(query: String): CompletableFuture<BufferedImage> {
            return requestImageUrl(query)
                .thenCompose(::requestImageData)
        }

        loadOneImage("dogs").thenAccept { image ->
            ImageIO.write(image, "png", FileOutputStream("dogs-1.png"))
            println("${image.width}x${image.height}")
        }.join()
    }

    @Test
    fun `02 Mehrere Bilder laden mit Futures`() {

        fun createCollage(query: String, count: Int): CompletableFuture<BufferedImage> {
            return requestImageUrls(query, count)
                .thenCompose { urls ->
                    val startFuture = CompletableFuture.completedFuture<List<BufferedImage>>(listOf())
                    urls.fold(startFuture) { lastFuture, url ->
                        lastFuture.thenCompose { images ->
                            requestImageData(url).thenApply { image ->
                                images + image
                            }
                        }
                    }
                }.thenApply(::combineImages)
        }

        createCollage("dogs", 20).thenAccept { image ->
            ImageIO.write(image, "png", FileOutputStream("dogs-n.png"))
            println("${image.width}x${image.height}")
        }.join()
    }


    fun requestImageUrl(query: String) = CompletableFuture<String>().also { future ->
        JerseyClient.qwant("q=$query&count=200")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    val url = urls.shuffled().firstOrNull() ?: return failed(IllegalStateException("No image found"))
                    future.complete(url)
                }

                override fun failed(throwable: Throwable) {
                    future.completeExceptionally(throwable)
                }
            })
    }

    fun requestImageUrls(query: String, count: Int) = CompletableFuture<List<String>>().also { future ->
        JerseyClient.qwant("q=$query&count=$count")
            .request()
            .async()
            .get(object : InvocationCallback<String> {
                override fun completed(response: String) {
                    val urls = JsonPath.read<List<String>>(response, "$..thumbnail").map { "https:$it" }
                    future.complete(urls)
                }

                override fun failed(throwable: Throwable) {
                    future.completeExceptionally(throwable)
                }
            })
    }

    fun requestImageData(imageUrl: String) = CompletableFuture<BufferedImage>().also { future ->
        JerseyClient.url(imageUrl)
            .request(MediaType.APPLICATION_OCTET_STREAM)
            .async()
            .get(object : InvocationCallback<InputStream> {
                override fun completed(response: InputStream) {
                    val image = ImageIO.read(response)
                    future.complete(image)
                }

                override fun failed(throwable: Throwable) {
                    future.completeExceptionally(throwable)
                }
            })
    }

    @AfterAll
    fun closeJerseyClient() {
        JerseyClient.close()
    }
}


