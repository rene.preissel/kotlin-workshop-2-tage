package p06_dsl._11_infix_operator.uebung

import java.lang.Math.abs

fun ggT(z1: Int, z2: Int): Int =
    when {
        z1 == 0 -> Math.abs(z2)
        z2 == 0 -> Math.abs(z1)
        else -> {
            var a = z1
            var b = z2
            do {
                val r = a % b
                a = b
                b = r
            } while (b != 0)

            Math.abs(a)
        }
    }


data class Bruch(val zaehler: Int, val nenner: Int) 

fun bruch(zaehler: Int, nenner: Int) =
    when {
        nenner < 1 ->
            throw IllegalArgumentException("Nenner nicht positiv: $nenner")
        zaehler == 0 ->
            Bruch(0, 1)
        else -> {
            val ggT = p06_dsl._11_infix_operator.loesung.ggT(abs(zaehler), nenner)
            Bruch(zaehler / ggT, nenner / ggT)
        }
    }

