package p06_dsl

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * ## Infix - Funktionen
 * Funktionen die nur genau einen Parameter benötigen können als _Infix_-Funktionen definiert werden:
 */
class _09_infix_intro {

    @Test
    fun `01 Infix Funktionen erhöhen die Lesbarkeit`() {
        val daysOhneInfix = mapOf("Mo".to("Monday"), "Tu".to("Tuesday"))

        val daysMitInfix = mapOf("Mo" to "Monday", "Tu" to "Tuesday")

        assertEquals(daysMitInfix, daysOhneInfix)
    }

    @Test
    fun `02 Eigene Infix Funktionen eigenen sich besonders im algebraischen Umfeld`() {
        class Circle(val x: Double, val y: Double, val radius: Double) {
            infix fun intersects(other: Circle): Boolean {
                val distanceX = this.x - other.x
                val distanceY = this.y - other.y
                val radiusSum = this.radius + other.radius
                return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum
            }
        }

        val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
        val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)
        assertTrue(c1 intersects c2)
    }


}


