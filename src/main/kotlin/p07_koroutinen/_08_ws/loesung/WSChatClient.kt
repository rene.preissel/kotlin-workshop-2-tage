@file:UseExperimental(KtorExperimentalAPI::class)

package p07_koroutinen._08_ws.loesung

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.websocket.WebSockets
import io.ktor.client.features.websocket.ws
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.header
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.time.delay
import mu.KotlinLogging
import java.time.Duration

fun main() = runBlocking {
    val client = HttpClient(CIO) {
        install(WebSockets)
    }


    repeat(5) { clientId ->
        launch {

            val request: HttpRequestBuilder.() -> Unit = {
                header("client.id", "client $clientId")
            }
            client.ws(host = "127.0.0.1", port = 8080, path = "/chat", request = request) {
                val logger = KotlinLogging.logger("client $clientId")

                coroutineScope {
                    val receiveJob = launch {
                        for (message in incoming) {
                            if (message is Frame.Text) {
                                logger.info(message.readText())
                            }
                        }
                    }
                    repeat(3) {
                        delay(Duration.ofSeconds(1))
                        outgoing.send(Frame.Text("Message from $clientId"))
                    }
                    delay(Duration.ofSeconds(2))
                    receiveJob.cancel()
                }

            }

        }
    }
}