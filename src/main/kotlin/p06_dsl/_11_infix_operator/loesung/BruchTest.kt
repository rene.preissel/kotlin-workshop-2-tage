package p06_dsl._11_infix_operator.loesung

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * AUFGABE
 * Erweitere die Klasse Bruch damit diese
 *  * per Infix-Funktion '2 ueber 3' erzeugt werden kann
 *  * Größer/Kleiner, Plus, Minus, Mal als Operatoren hat
 *  * ein Property Reziprok/Kehrwert als `lazy` Property bereitstellt
 *  * Konstanten für Standardwerte: 0,1,10 bereitstellt
 *  * Optional: eine Konvertierungs-Funktion von String (1.25) implementiert
 *  * Optional: einen Invoke Operator im Companion-Objekt als Ersatz für den Konstruktor bereitstellt
 */
class BruchTest {
    @Test
    fun `anlegen und kuerzen`() {
        val bruch = Bruch(10, 5)
        assertEquals(2, bruch.zaehler)
        assertEquals(1, bruch.nenner)
    }

    @Test
    fun `infix zum anlegen`() {
        val bruch = 5 ueber 10
        assertEquals(Bruch(1, 2), bruch)
    }

    @Test
    fun `Vegleiche`() {
        val bruch1 = 5 ueber 10
        val bruch2 = 3 ueber 9


        assertTrue(bruch1 > bruch2)
        assertTrue(bruch2 < bruch1)
    }

    @Test
    fun `Rechnen`() {
        val bruch1 = 5 ueber 10
        val bruch2 = 3 ueber 9

        val summe = bruch1 + bruch2
        val differenz = bruch1 - bruch2
        val produkt = bruch1 * bruch2

        assertEquals(5 ueber 6, summe)

        assertEquals(1 ueber 6, differenz)

        assertEquals(1 ueber 6, produkt)
    }

    @Test
    fun `lazy reziprok`() {
        val bruch = 5 ueber 10
        val reziprok = bruch.reziprok
        assertEquals(2 ueber 1, reziprok)
    }

    @Test
    fun `Konstanten`() {
        assertEquals(Bruch(0, 1), Bruch.ZERO)
        assertEquals(Bruch(1, 1), Bruch.ONE)
        assertEquals(Bruch(10, 1), Bruch.TEN)
    }

    @Test
    fun `Konvertieren`() {
        assertEquals(5 ueber 10, "0.5".toBruch())
        assertEquals(155 ueber 100, "1.55".toBruch())
    }
}


