package p04_extensions_lambdas

import org.h2.jdbcx.JdbcDataSource
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows


/**
 * ### Übung: Einsatz von Standard-Scope-Funktionen
 */
class EinsatzVonLambdas {
    val JDBC_URL = "jdbc:h2:mem:singleton;DB_CLOSE_DELAY=-1"
    @Test
    fun `01 Einsatz von Standard-Scope-Funktionen`() {

        // AUFGABE
        // Vereinfache den nachfolgenden Code durch den Einsatz von Standard-Funktionen

        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"

        val connection = datasource.connection
        try {
            val statement = connection.createStatement()
            try {
                statement.executeUpdate(
                    """
                    CREATE TABLE PERSON(
                        id int auto_increment primary key,
                        firstname varchar(100),
                        lastname varchar(100)
                    )
                """.trimIndent()
                )
            } finally {
                statement.close()
            }
        } finally {
            connection.close()
        }

        assertTableExist("PERSON")
    }

    @Test
    fun `02 Neues Lambda with Receiver erstellen`() {

        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"

        // AUFGABE
        // Erstelle die Funktion `withStatement` als Extension-Funktion an einer Datasource
        // Die neue Funktion soll ein Lambda übergeben bekommen bei dm ein Statement als This-Zeiger gesetzt ist.
        // Der nachfolgende Code soll anschliessend laufen

//        datasource.withStatement {
//            executeUpdate(
//                """
//                    CREATE TABLE PERSON(
//                        id int auto_increment primary key,
//                        firstname varchar(100),
//                        lastname varchar(100)
//                    )
//                """.trimIndent()
//            )
//        }

        assertTableExist("PERSON")
    }

    @Test
    fun `03 Nochmal Standard-Scope-Funktionen`() {
        data class Parameter(val p1: String, val p2: String, val p3: String?)

        // AUFGABE
        // Vereinfache die nachfolgende Funktion unter Einsatz von Standard-Funktionen
        fun parseArgs(vararg args: String): Parameter {
            if (args.size != 2 && args.size != 3) {
                throw IllegalStateException("2 or 3 parameter required")
            }

            val (p1, p2) = args


            val p3 = if (args.size == 3) {
                args[2]
            } else {
                null
            }

            return Parameter(p1, p2, p3)
        }

        assertEquals(Parameter("a", "b", "c"), parseArgs("a", "b", "c"))
        assertEquals(Parameter("a", "b", null), parseArgs("a", "b"))
        assertThrows<IllegalStateException> {
            parseArgs("a", "b", "c", "d")
        }
    }


    @BeforeEach
    fun dropAllObjects() {
        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"

        val connection = datasource.connection
        try {
            val statement = connection.createStatement()
            try {
                statement.executeUpdate("DROP ALL OBJECTS")
            } finally {
                statement.close()
            }
        } finally {
            connection.close()
        }
    }

    fun assertTableExist(tbl: String) {
        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"
        val connection = datasource.connection
        try {
            val resultSet = connection.metaData.getTables(null, null, tbl, null)
            try {
                assertTrue(resultSet.next()) {
                    "Table $tbl does not exist"
                }
            } finally {
                resultSet.close()
            }
        } finally {
            connection.close()
        }
    }

}
