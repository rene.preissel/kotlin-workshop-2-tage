package p04_extensions_lambdas

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


object Metrics {
    fun collect(name: String, duration: Long) {
        println("Metric $name: $duration ms" )
    }
}

inline fun <T> timedWithMetric(metricsName: String? = null, body: () -> T): T {
    val start = System.currentTimeMillis()
    try {
        return body()
    } finally {
        val duration = System.currentTimeMillis() - start
        println(duration)
        if (metricsName != null) {
            Metrics.collect(metricsName, duration)
        }
    }
}

//Siehe decompilierter Code für Unterschied

fun add1(a: Int, b: Int) = timedWithMetric("add") {
    a + b
}

fun add2(a: Int, b: Int) = timedWithMetric {
    a + b
}

fun main(args: Array<String>) {
    add1(1, 2)
    add2(1, 2)
}

