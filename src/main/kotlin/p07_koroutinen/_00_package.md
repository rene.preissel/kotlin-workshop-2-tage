# Koroutinen

## Warum

![](_utilization.png)    

---

## Blocking vs Non-Blocking

![](_blocking.png)    

---

## Beispiel: Kollage

![](_turtle.png)    

 ```kotlin
 val collage = createCollage("turtle", 20)
 ```