package p06_dsl

import kotlinx.html.div
import kotlinx.html.h1
import kotlinx.html.li
import kotlinx.html.stream.appendHTML
import kotlinx.html.ul
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * ## Operatoren redefinieren
 * * Kotlin erlaubt das Redefinieren von vorhandenen https://kotlinlang.org/docs/reference/operator-overloading.html.
 * * Das Definieren von ganz neuen Operatoren ist nicht möglich.
 * * Operatoren zu redefinieren kann in algebraischen Domänen sinnvoll sein.
 * * Missbrauch führt zu schwer verständlichen Code und sollte besser durch eigene Infix-Funktionen ausgetauscht werden
 */
class _09_operator_intro {

    @Test
    fun `01 Eigenen Operator definieren`() {
        class Circle(val x: Double, val y: Double, val radius: Double) {
            infix fun intersects(other: Circle): Boolean {
                val distanceX = this.x - other.x
                val distanceY = this.y - other.y
                val radiusSum = this.radius + other.radius
                return distanceX * distanceX + distanceY * distanceY <= radiusSum * radiusSum
            }

            operator fun rem(other: Circle): Boolean = intersects(other)
        }

        val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
        val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)
        assertTrue(c1 % c2)
    }

    @Test
    fun `02 Vergleichs-Operatoren redefinieren`() {
        class Circle(val x: Double, val y: Double, val radius: Double) : Comparable<Circle> {
            override operator fun compareTo(other: Circle): Int = (this.radius - other.radius).let {
                when {
                    it < -0.0001 -> -1
                    it > 0.0001 -> +1
                    else -> 0
                }
            }

            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Circle

                if (compareTo(other) != 0) return false

                return true
            }

            override fun hashCode(): Int {
                return radius.hashCode()
            }
        }

        val c1 = Circle(x = 100.0, y = 100.0, radius = 50.0)
        val c2 = Circle(x = 75.0, y = 75.0, radius = 5.0)

        assertFalse(c1 == c2)
        assertTrue(c1 > c2)
    }


    @Test
    fun `03 Einsatz bzw Missbrauch bei kotlinx-html`() {
        System.out.appendHTML()
            .div {
                h1 {
                    +"Features"
                }
                ul {
                    li { +"Extension Functions" }
                    li { +"Lambdas with receiver" }
                    li { +"Operator overloading" }
                }
            }
    }
}


