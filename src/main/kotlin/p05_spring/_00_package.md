# Package p05_spring

* Spring bringt seit der Version 5.0.0 und SpringBoot seit der Version 2.0.0 eigene
  Extensions um den Umgang mit der Spring-API zu vereinfachen  
* Damit Kotlin und Spring gut zusammenarbeiten sollten/müssen alle Beans als `open` definiert werden.
  Alternativ nimmt man ein entsprechendes _Gradle_ oder _Maven_ Plugin.  
* Es gibt ein neues Projekt [spring-fu](https://github.com/spring-projects/spring-fu), 
 in dem unter anderen eine reine Kotlin-Konfiguration ohne Annotations entwickelt wird 
