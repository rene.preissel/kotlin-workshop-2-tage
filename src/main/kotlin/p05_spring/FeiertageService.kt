package p05_spring

import java.time.LocalDate

data class Feiertag(val name: String, val tag: LocalDate)

class FeiertageService {

    val GEMEINSAME_FEIERTAGE: Set<(LocalDate, LocalDate) -> Feiertag> = setOf(
        fixesDatum("Neujahr", 1, 1),
        fixesDatum("Tag der Arbeit", 1, 5),
        fixesDatum("Tag der Deutschen Einheit", 3, 10),
        fixesDatum("1. Weihnachtstag", 25, 12),
        fixesDatum("2. Weihnachtstag", 26, 12),
        relativZumOsterSonntag("Karfreitag", -2),
        relativZumOsterSonntag("Ostermonntag", 1),
        relativZumOsterSonntag("Christi Himmelfahrt", 39),
        relativZumOsterSonntag("Pfingstmontag", 50)
    )

    val FEIERTAGE_BAYERN = GEMEINSAME_FEIERTAGE + setOf(
        fixesDatum("Heilige Drei Könige", 6, 1),
        fixesDatum("Mariä Himmelfahrt", 15, 8),
        fixesDatum("Allerheiligen", 1, 11),
        relativZumOsterSonntag("Fronleichnam", 60)
    )

    val FEIERTAGE_HAMBURG = GEMEINSAME_FEIERTAGE + setOf(
        fixesDatum("Reformationstag", 31, 10)
    )

    val FEIERTAGE_BUNDESLAENDER = mapOf(
        "Bayern" to FEIERTAGE_BAYERN,
        "Hamburg" to FEIERTAGE_HAMBURG
    )

    fun feiertage(von: LocalDate, bis: LocalDate, bundesland: String? = null): List<Feiertag> {
        if (von.year != bis.year) throw IllegalArgumentException("Das Jahr muss gleich sein: ${von.year} <> ${bis.year}")

        val jahresAnfang = LocalDate.of(von.year, 1, 1)
        val osterSonntag = berechneOsterSonntag(jahresAnfang.year)

        val feiertagsListe = bundesland?.let {
            FEIERTAGE_BUNDESLAENDER[it]
        } ?: GEMEINSAME_FEIERTAGE

        val feiertageImJahr = feiertagsListe.map { it(jahresAnfang, osterSonntag) }.sortedBy { it.tag }
        return feiertageImJahr.dropWhile { it.tag < von }.takeWhile { it.tag <= bis }
    }

    private fun berechneOsterSonntag(year: Int): LocalDate {
        //http://openbook.rheinwerk-verlag.de/javainsel9/javainsel_15_006.htm#t2t35
        val i = year % 19;
        val j = year / 100;
        val k = year % 100;

        val l = (19 * i + j - (j / 4) - ((j - ((j + 8) / 25) + 1) / 3) + 15) % 30;
        val m = (32 + 2 * (j % 4) + 2 * (k / 4) - l - (k % 4)) % 7;
        val n = l + m - 7 * ((i + 11 * l + 22 * m) / 451) + 114;

        val month = n / 31;
        val day = (n % 31) + 1;

        return LocalDate.of(year, month, day);
    }

    private fun fixesDatum(name: String, tag: Int, monat: Int): (LocalDate, LocalDate) -> Feiertag =
        { jahr, _ -> Feiertag(name, jahr.withDayOfYear(tag).withMonth(monat)) }

    private fun relativZumOsterSonntag(name: String, tage: Int): (LocalDate, LocalDate) -> Feiertag =
        { _, osterSonntag -> Feiertag(name, osterSonntag.plusDays(tage.toLong())) }

}
