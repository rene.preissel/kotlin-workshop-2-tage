@file:Suppress("PackageDirectoryMismatch")

package p04_extensions_lambdas.util

import java.net.URL

//Erweiterung der String-Klasse um Funktion toURL()
fun String.toURL() = URL(this)