package p04_extensions_lambdas;

import com.fasterxml.jackson.databind.ObjectMapper;
import p04_extensions_lambdas._05_standard_lambda_scope_funktionen.Person;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class _09_typeerasure {
    public static void main(String[] args) throws IOException {

        //Beispiel für Type-Erasure bei Arrays
        List<Integer> numbers = Arrays.asList(1, 2, 3);
        Integer[] intArray = numbers.toArray(new Integer[0]);

        String json = "{" +
                "\"vorname\": \"Rene\",\n" +
                "\"nachname\": \"Preissel\"" +
                "}";

        //Beispiel für Type-Erasure bei Jackson
        Person person = new ObjectMapper().readValue(json, Person.class);
    }
}
