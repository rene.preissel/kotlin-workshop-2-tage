package p07_koroutinen._08_ws.uebung

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.request.header
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import mu.KotlinLogging
import java.time.Duration

/**
 * AUFGABE:
 * Teil 1:
 * Erweitere den Server um einen weiteren Webservice-Endpoint: '/chat'
 * Dieser soll alle Nachrichten die er von einem Client erhält an alle anderen Clients weiterleiten, aber nicht an sich selbst
 * Teil 2:
 * Schreibe einen neuen WSChatClient der fünf Verbindungen zu dem Chat-Webservice aufbaut und jeweils drei Nachrichten an den Server schickt.
 * Teil 3 (Optional)
 * Baue einen Timeout in allen Clients ein, der zur Beendigung des Clients führt, wenn fünf Sekunden keine Nachrichten eintreffen
 * Tipp: Es gibt ein `onTimeout` für die `select`-Operation. Es gibt ein `whileSelect` welches solange läuft bis ein Event false liefert
 */
fun Application.webSockets() {
    val logger = KotlinLogging.logger("server")

    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(60)
    }

    routing {
        webSocket("/ping") {
            val clientId = call.request.header("client.id")
            logger.info("Connect $clientId")

            for (message in incoming) {
                if (message is Frame.Text) {
                    val text = message.readText()
                    logger.info("Receive message $text from $clientId")
                    outgoing.send(Frame.Text("pong to $clientId"))
                }
            }
            logger.info("Disconnect $clientId")

        }
    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080, module = Application::webSockets)
    server.start(wait = true)

}