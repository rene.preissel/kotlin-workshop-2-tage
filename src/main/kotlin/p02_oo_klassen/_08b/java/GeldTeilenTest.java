package p02_oo_klassen._08b.java;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GeldTeilenTest {

    @Test
    public void geldTeilen() {

        GeldBetrag b = new GeldBetrag(47.11);

        assertThat(b.getInCent()).isEqualTo(4711);

        assertThat(b.mal(2).getInCent()).isEqualTo(9422);
        assertThat(b.plus(new GeldBetrag(0.89)).getInCent()).isEqualTo(4800);

        assertThat(new GeldBetrag(47.00).plus(new GeldBetrag(0.11)))
                .isEqualTo(b);

        QuotientMitRest quotientMitRest = b.teile(4);

        assertThat(quotientMitRest.getQuotient()).isEqualTo(new GeldBetrag(11.77));
        assertThat(quotientMitRest.getRest()).isEqualTo(new GeldBetrag(0.03));
        assertThat(quotientMitRest.getQuotient().mal(4).plus(quotientMitRest.getRest()))
                .isEqualTo(b);
    }
}
