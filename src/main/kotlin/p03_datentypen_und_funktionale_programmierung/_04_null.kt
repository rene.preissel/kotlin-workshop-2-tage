package p03_datentypen_und_funktionale_programmierung

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test

/**
 * ## Kotlin und die `null`-Pointer und die *Smart Casts*
 *
 * Ein wesentlicher Unterschied zu Java ist der Umgang mit `null`.
 * In der Regel darf Variablen oder Parametern kein `null`-Wert
 * zugewiesen werden, außer dies ist explizit deklariert
 * (mit der `?`-Notation).
 * Dadurch erreicht man eine viel höhere Sicherheit vor
 * `NullPointerException`s, denn wo kein null-Pointer hin soll,
 * da kommt auch keiner hin.
 *
 * Wo es aber erforderlich ist mit Null-Variablen zu arbeiten,
 * können die als Optional deklariert werden (mit `?`).
 * Kotlin bietet eine Reihe von nützlichen Notationen
 * und Funktionen, die dafür sorgen, dass der Code nicht
 * mit länglichen `null`-Check aufgebläht werden muss.
 */
class _04_null {

    /**
     * ### Datentypen
     *
     * Jeder Datentyp entspricht einer Kotlin-Klasse,
     * und umfasst Instanzen dieser Klasse, nicht
     * aber `null`.
     *
     * Erst durch ein angehängtes `?` wird der Wertebereich
     * um `null` erweitert.
     */
    @Test
    fun `01 Variablen können nicht "genullt" werden`() {

        var s = "ein String"


        // s = null geht nicht
    }

    /**
     * ## Optionale Datentypen mit "`?`"
     *
     * Jedem Datentyp kann ein `?` angehängt werden.
     * Sind ist zusätzlich zum normalen Wertebereich aus `null` zulässig.
     */
    @Test
    fun `02 Optionale Typen mit "?"`() {

        var s2: String? = "ein String"

        s2 = null

        assertThat(s2 == null).isTrue()
    }

    /**
     * ## null-Pointer aus Java-Aufrufen
     *
     * Methoden aus Java-Bibliothen kann man in der Regel
     * nicht ansehen, ob sie `null` liefern können.
     * Kotlin geht damit wie folgt um.
     *
     * * Bei `?`-Typen wird das null durchgereicht.
     *
     * * `?` wird auch von der Type-Inferenz geliefert.
     *
     * * Bei `null`-freien Typen wird zum Zeitpunkt der Zuweisung eine
     *   Exception durchgereicht.
     */
    @Test
    fun `03 null-Values als Rückgabewerte von Java-Klassen`() {


        println(System.getProperties())
        val p1: String? = System.getProperty("os.name")


        assertThat(p1).isNotEmpty()


        val p2: String? = System.getProperty("gipsnich")


        assertThat(p2).isNull()


        val p3 = System.getProperty("gipsnich")


        assertThat(p3).isNull()
        assertThat(p3 is String?)
            .isTrue()
            .`as`(
                "Die Type Inference geht davon aus, " +
                        "dass null geliefert werden könnte."
            )

        assertThatThrownBy {

            val p4: String = System.getProperty("gipsnich")


            println(p3)
        }.hasMessageContaining("must not be null")


    }

    /**
     * ## Der Elvis-Operator
     */
    @Test
    fun `04 Der Elvis-Operator`() {

        for (el in listOf("Hamlet", null)) {
            val name =
                el ?: "<Name>"

            println(name)
            assertThat(name).isNotEmpty()
        }
    }


    /**
     * Da das Werfen einer Exception auch eine Ausdruck ist,
     * kann, kann es auch im Elvis-Operator für den Null-Fall genutzt werden.
     */
    @Test
    fun `05 Exceptional Elvis`() {

        fun frissOderStirb(food: String?) =

            food ?: throw RuntimeException("Das ist das Ende")



        assertThat(frissOderStirb("Apfel")).isEqualTo("Apfel")

        assertThatThrownBy {
            frissOderStirb(null)
        }.hasMessageContaining("Ende")


    }


    /**
     * ## Der `?.`-Operator
     */
    @Test
    fun `06 Optionale Felder referenzieren`() {

        data class LinkedList(val value: Any, val tail: LinkedList?)

        val linkedList = LinkedList(1, LinkedList(2, LinkedList(3, null)))

        // linkedList.tail.tail.value)  // nicht erlaubt, wg. null

        assertThat(
            linkedList.tail?.tail?.value
        ).isEqualTo(3)

        assertThat(
            linkedList.tail?.tail?.tail?.tail?.value
        ).isNull()
    }


    /**
     * ## *Smart Casts*
     *
     * Die Type-Inference von Kotlin geht über die Deklaration von Variablen hinaus.
     * Wenn in Fallunterscheidungen (z. B. `if`, `when`) beispielsweise,
     * `x != null` oder `x is String` geprüft wird,
     * dann wird `x` in dem entsprechenden Zweig auch so gecasted.
     *
     *
     */
    @Test
    fun `07 Smart Casts bei null Werten`() {

        for (s in listOf("hallo", null, "Welt")) {
            if (
                s != null
            ) {
                println(s.toUpperCase())
                assertThat(s is String)
                // println(s?.toUpperCase())  // Hier nicht nötig, weil Kotlin hier weiss,
                // dass s nicht null ist.
            } else {
                // println(s.toUpperCase())   // Hier nicht möglich, weil s ja null ist.
                println("No value")
            }
        }
    }


    @Test
    fun `08 Smart Casts mit beliebigen Typen`() {

        for (c: Collection<Int> in listOf(
            listOf(3, 4, 5),
            setOf(5, 6),
            listOf(7, 8, 9)
        )) {
            if (c is List) {
                // Hier ist c: List<Int>
                println("A list starting with ${c[0]}")
            } else {
                // Hier ist c: Collection<Int>
                // println("A list starting with ${c[0]}")
                println("A collection with ${c.size} elements")

            }
        }

        for (el in listOf("abc", true, 42)) {
            when (el) {
                is String -> println(el.toUpperCase())
                is Boolean -> println(if (el) "Ja" else "Nein")
                else -> println(el)
            }
        }

    }

    @Test
    fun `09 Normale Casts und Nullables`() {
        val n: Number = 42

        //Exception wenn Cast nicht möglich
        val i = n as Int

        //Null wenn Cast nicht möglich
        val l = n as? Long
        assertThat(l).isNull()
    }

}
