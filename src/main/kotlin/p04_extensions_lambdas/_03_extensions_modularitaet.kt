package p04_extensions_lambdas

import org.junit.jupiter.api.Test
import kotlin.math.PI


interface Loggable {
    val loggerName: String
        get() = this.javaClass.simpleName ?: "unknown"
}

fun Loggable.info(msg: String) {
    println("$loggerName: $msg")
}

/**
 * ## Extensions Funktionen dienen zur Modularisierung / Erweiterbarkeit von Libraries/Framework
 */
class _03_extensions_modularitaet {
    @Test
    fun `01 Extensions Funktionen erweitern Libraries`() {

        //Das könnte in einem anderen Modul stehen oder nachträglich implementiert werden
        fun <T> Loggable.withTracing(prefix: String, block: () -> T): T = try {
            info("$prefix - begin")
            block()
        } finally {
            info("$prefix - end")
        }

        class Circle(val radius: Double) : Loggable {
            init {
                info("Init circle")
            }

            val area: Double
                get() = withTracing("area") {
                    PI * radius * radius
                }
        }

        val circle = Circle(10.0)
        println(circle.area)
    }
}

