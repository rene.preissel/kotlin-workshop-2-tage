@file:Suppress("PackageDirectoryMismatch") @file:JvmName("KtorExample2Kt")

package p06_dsl.modules

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.html.body
import kotlinx.html.h1


fun Application.module1() {
    routing {
        get("/hello1") {
            call.respondTextWriter {
                appendln("Hello")
            }
        }

        //Zugriff auf URL Parameter
        get("/hello2/{name}") {
            val name = call.parameters["name"]
            if (name == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                call.respondTextWriter {
                    appendln("Hello $name")
                }
            }
        }

        //Integration mit Kotlinx.html im eigenem Modul
        get("/hello3/{name}") {
            val name = call.parameters["name"] ?: throw IllegalStateException("name missing")
            call.respondHtml {
                body {
                    h1 {
                        text("Hallo $name")
                    }
                }
            }
        }
    }


}

class Person(val vorname: String, val nachname: String)

fun Application.module2() {
    //Weiteres Feature installieren um JSon zurückzugeben
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }
    routing {
        get("/person") {
            call.respond(Person("Rene", "Preissel"))
        }

    }
}

fun main() {
    val server = embeddedServer(Netty, port = 8080) {
        module1()
        module2()
    }
    server.start(wait = true)
}