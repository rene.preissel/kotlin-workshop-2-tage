import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension
import org.springframework.boot.gradle.dsl.SpringBootExtension

val kotlinCoroutineVersion by extra("1.1.0")
val ktorVersion: String by extra { "1.0.1" }
val kodeinVersion: String by extra { "6.0.1" }


plugins {
    val kotlinVersion = "1.3.11"

    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.allopen") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
    id("org.springframework.boot") version "2.1.0.RELEASE"
    id("org.jetbrains.dokka") version "0.9.17"
}

apply {
    plugin("io.spring.dependency-management")
    plugin("org.jetbrains.dokka")
}


group = "de.e2"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
    maven("https://dl.bintray.com/kotlin/ktor")
    maven("https://dl.bintray.com/kotlin/kotlinx.html")
}

springBoot {
    mainClassName = "p03_extensions_reified.s0x_spring.springboot.dsl.HelloWorldControllerDSLKt"
}

configurations.forEach {
    it.exclude(group = "junit", module = "junit")
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinCoroutineVersion")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:$kotlinCoroutineVersion")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:$kotlinCoroutineVersion")
    compile("org.springframework.boot:spring-boot-starter-webflux")
    compile("org.springframework.boot:spring-boot-starter-data-jpa")
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")

    compile("org.glassfish.jersey.core:jersey-client:2.26")
    compile("org.glassfish.jersey.inject:jersey-hk2:2.26")
    compile("org.glassfish.jersey.media:jersey-media-json-jackson:2.26")
    compile("com.jayway.jsonpath:json-path:2.3.0")
    runtime("javax.activation:activation:1.1.1")

    compile("io.github.microutils:kotlin-logging:1.6.22")

    compile("io.ktor:ktor-server-core:$ktorVersion")
    compile("io.ktor:ktor-server-netty:$ktorVersion")
    compile("io.ktor:ktor-locations:$ktorVersion")
    compile("io.ktor:ktor-websockets:$ktorVersion")

    compile("io.ktor:ktor-client-websocket:$ktorVersion")
    compile("io.ktor:ktor-client-cio:$ktorVersion")
    compile("io.ktor:ktor-html-builder:$ktorVersion")
    compile("io.ktor:ktor-gson:$ktorVersion")

    compile("org.kodein.di:kodein-di-generic-jvm:$kodeinVersion")
    compile("org.kodein.di:kodein-di-conf-jvm:$kodeinVersion")

    compile("com.google.code.gson:gson:2.8.5")
    compile("com.xenomachina:kotlin-argparser:2.0.7")
    compile("com.h2database:h2:1.4.196")

    implementation("org.junit.jupiter:junit-jupiter-api:5.3.1")
    runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.3.1")
    implementation("org.assertj:assertj-core:3.11.1")

}

tasks {
    val dokka by named<DokkaTask>("dokka") {
        includes =
                listOf(
                    "README.md",
                    "src/main/kotlin/p02_oo_klassen/_00_package.md"
                )
        outputFormat = "html"
    }

    register("browseDocs") {
        dependsOn(dokka)
        doLast {
            logger.info("echo firefox $buildDir/dokka/$name")
            Runtime.getRuntime().exec("firefox ${dokka.outputDirectory}/${project.name}/index.html")
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs = listOf("-Xjsr305=strict -Xuse-experimental=kotlin.Experimental")
}

tasks {
    val hello by registering {
        doLast {
            println("Hello from Creating")
        }
    }
}