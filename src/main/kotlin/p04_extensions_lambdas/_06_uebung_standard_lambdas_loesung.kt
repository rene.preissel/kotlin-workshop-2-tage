package p04_extensions_lambdas

import org.h2.jdbcx.JdbcDataSource
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.sql.Statement
import javax.sql.DataSource


/**
 * ### Übung: Einsatz von Standard-Scope-Funktionen

 */
class EinsatzVonLambdasLoesung {
    val JDBC_URL = "jdbc:h2:mem:singleton;DB_CLOSE_DELAY=-1"
    @Test
    fun `01 Einsatz von Standard-Scope-Funktionen`() {

        val datasource = JdbcDataSource().apply {
            setUrl(JDBC_URL)
            user = "sa"
        }

        datasource.connection.use { connection ->
            connection.createStatement().use { statement ->
                statement.executeUpdate(
                    """
                CREATE TABLE PERSON(
                    id int auto_increment primary key,
                    firstname varchar(100),
                    lastname varchar(100))
                """.trimIndent()
                )
            }
        }
        assertTableExist("PERSON")
    }

    @Test
    fun `02 Neues Lambda with Receiver erstellen`() {

        fun DataSource.withStatement(block: Statement.() -> Unit) = connection.use {connection ->
            connection.createStatement().use {statement ->
                statement.block()
            }
        }

        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"

        datasource.withStatement {
            executeUpdate(
                """
                    CREATE TABLE PERSON(
                        id int auto_increment primary key,
                        firstname varchar(100),
                        lastname varchar(100)
                    )
                """.trimIndent()
            )
        }

        assertTableExist("PERSON")
    }

    @Test
    fun `03 Nochmal Standard-Scope-Funktionen`() {
        data class Parameter(val p1: String, val p2: String, val p3: String?)

        fun parseArgs(vararg args: String): Parameter {
            val (p1, p2) = args.takeIf { it.size in 2..3 }
                    ?: throw IllegalStateException("2 or 3 parameter required")

            val p3 = args.takeIf { it.size == 3 }?.let {
                it[2]
            }



            return Parameter(p1, p2, p3)
        }

        assertEquals(Parameter("a", "b", "c"), parseArgs("a", "b", "c"))
        assertEquals(Parameter("a", "b", null), parseArgs("a", "b"))
        assertThrows<IllegalStateException> {
            parseArgs("a", "b", "c", "d")
        }
    }


    @BeforeEach
    fun dropAllObjects() {
        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"

        val connection = datasource.connection
        try {
            val statement = connection.createStatement()
            try {
                statement.executeUpdate("DROP ALL OBJECTS")
            } finally {
                statement.close()
            }
        } finally {
            connection.close()
        }
    }

    fun assertTableExist(tbl: String) {
        val datasource = JdbcDataSource()
        datasource.setURL(JDBC_URL)
        datasource.user = "sa"
        val connection = datasource.connection
        try {
            val resultSet = connection.metaData.getTables(null, null, tbl, null)
            try {
                assertTrue(resultSet.next()) {
                    "Table $tbl does not exist"
                }
            } finally {
                resultSet.close()
            }
        } finally {
            connection.close()
        }
    }

}
