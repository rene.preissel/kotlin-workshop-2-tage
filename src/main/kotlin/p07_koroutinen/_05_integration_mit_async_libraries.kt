package p07_koroutinen

import kotlinx.coroutines.future.await
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.awt.image.BufferedImage
import java.io.InputStream
import java.util.concurrent.CompletableFuture
import javax.imageio.ImageIO
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


class _05_integration_mit_async_libraries {


    /**
     * Über ein Continuation-Objekt kann man mit asynchronen Libraries integrieren
     */
    @Test
    fun `01 suspendCoroutine suspendiert die aktuelle Koroutine`() {

        suspend fun requestImageData(imageUrl: String) = suspendCoroutine<BufferedImage> { cont ->
            JerseyClient.url(imageUrl)
                .request(MediaType.APPLICATION_OCTET_STREAM)
                .async()
                .get(object : InvocationCallback<InputStream> {
                    override fun completed(response: InputStream) {
                        val image = ImageIO.read(response)
                        cont.resume(image)
                    }

                    override fun failed(throwable: Throwable) {
                        cont.resumeWithException(throwable)
                    }
                })
        }

        runBlocking {
            val image = requestImageData("https://etosquare.de/img/rene2018.jpg")
            println("${image.width}x${image.height}")
        }
    }

    @Test
    fun `02 Es gibt Integrationen mit CompletableFuture und anderen Async-Libraries`() {

        fun requestImageData(imageUrl: String): CompletableFuture<BufferedImage> {
            val result = CompletableFuture<BufferedImage>()
            JerseyClient.url(imageUrl)
                .request(MediaType.APPLICATION_OCTET_STREAM)
                .async()
                .get(object : InvocationCallback<InputStream> {
                    override fun completed(response: InputStream) {
                        val image = ImageIO.read(response)
                        result.complete(image)
                    }

                    override fun failed(throwable: Throwable) {
                        result.completeExceptionally(throwable)
                    }
                })

            return result;
        }

        val completableFuture = requestImageData("https://etosquare.de/img/rene2018.jpg")
        runBlocking {
            //Nicht blockierendes Warten auf das Future
            val image = completableFuture.await()
            println("${image.width}x${image.height}")
        }
    }
}


